//  main.cpp
//  MP
//
//  Created by Daniele on 11/02/2017.
//  Copyright © 2017 DL. All rights reserved.

#include "CLI11.hpp"
#include "Equilibrium.hpp"

volatile sig_atomic_t kl = true;
volatile sig_atomic_t signal_save = false;
volatile sig_atomic_t signal_USR2 = false;

using namespace arma;

// Signal handler for SIGINT and SIGTERM. Lets the loop finish
void signal_callback_handler(int signum) {
  std::cerr << "\nCaught signal interrupt\n";
  kl = false;
}

// Signal handler for SIGUSR1. Saves the hdf file
void signal_save_handler(int signum) {
  std::cerr << "\nCaught signal save\n\n";
  signal_save = true;
}

void signal_inc_lambda_handler(int signum) {
  std::cerr << "\nCaught signal USR2\n\n";
  signal_USR2 = true;
}

void evoke_exit(Params &P, bool so) {
  P.save_results(so);
  std::cout << "Exit with code 1. Time: " << P.get_timestamp() << "\n";
  exit(1);
}

int main(int argc, char *argv[]) {

  // +------------------*
  // | Signal listeners |
  // +------------------*
  signal(SIGINT, signal_callback_handler);
  signal(SIGTERM, signal_callback_handler);
  signal(SIGUSR1, signal_save_handler);
  signal(SIGUSR2, signal_inc_lambda_handler);

  // +-----------------------------------------------*
  // |  Inputs from external files and coPand line  |
  // +-----------------------------------------------*
  long int n_input = 50., a_input = 90., l_input = 50., cl_input = 0.0;
  uword stp = 0;
  int solver_mode = 1;
  bool geom_config = false, load_prev = false, arc_sign = 0, with_diag = true;
  double prestrain = 0;
  std::string geom_dir = "Input/geom_config.csv", exe_dir = "", ID = "";

  CLI::App app{"Nodes"};

  app.add_option("-n,--nodes", n_input, "number of nodes per side");
  app.add_option("-m,--mode", solver_mode,
                 "algorithm. 0 = displacement control; 1 = arc length control (default)");
  app.add_option("-s,--sign", arc_sign, "sign of the direction taken by the arc length method");
  app.add_option("-a,--angles", a_input, "pulling angle");
  app.add_option("-c,--centrality", l_input, "centrality");
  app.add_option("-l,--length", cl_input, "stiffer element length");
  app.add_option("-w,--with_diag", with_diag, "with diagonals or not (default := true)");
  app.add_option("-p,--prestrain", prestrain, "prestrain in the elements");
  app.add_option("-I,--ID", ID, "ID of the program");
  app.add_option("-d,--directory", exe_dir, "directory of the executable");
  app.add_option("-L,--Load_previous", load_prev,
                 "Start from previous simulation. 0 = false (default), 1 = true");
  app.add_option("--loading_step", stp, "loading step (default := 0)");
  app.add_option("-g,--initial_config", geom_config,
                 "0 = membrane on xy plane (default); 1 = membrane geometry loaded from csv file");
  app.add_option("-f,--geometry_file", geom_dir,
                 "Directory of the initial geometry file. (default := /geom_config.csv) ");

  CLI11_PARSE(app, argc, argv);

  Params P(n_input, ID, exe_dir);

  P.print_run_info();

  if (load_prev) {
    P.load_results();
    try {
      // P.set_params(n_input);
    } catch (...) {
      std::cerr << "Unable to set parameters from loading";
    }
  }
  double cstart = 0, clength = 0, cstiff = 0, theta_pull = 0;

  // +---------------------------------------------+
  // |   Converting inputs in available doubles    |
  // +--------------------and----------------------+
  // |          Checking optional values           |
  // +---------------------------------------------+
  // Pulling angle
  if (!std::isnan(P.opt_alpha))
    a_input = P.opt_alpha;
  theta_pull = M_PI - M_PI_2 / 90 * (a_input);

  // Where is the membrane pulled along the x-axis
  if (!std::isnan(P.opt_cd))
    cstart = floor(P.opt_cd / P.l0) * P.l0;
  else
    cstart = floor(((double)l_input / 100.0 * P.sidex) / P.l0) * P.l0;

  std::cout << "cstart: " << cstart << std::endl;
  // Length of the dragline (0.0 if it doesn't exist)
  if (!std::isnan(P.opt_cl))
    clength = floor(P.opt_cl / P.l0) * P.l0;
  else
    clength = floor(((double)cl_input / 100.0 * P.sidex) / P.l0) * P.l0;
  std::cout << "clength: " << clength << std::endl;

  if (!std::isnan(P.opt_cE))
    cstiff = P.opt_cE / P.EA;

  if (!with_diag)
    P.adjust_EA_poisson();

  // if (theta_pull > M_PI_2 * 1.01)
  //  arc_sign = 1;

  // +---------------------------------------------+
  // |        Creation of Nodes and Springs        |
  // +---------------------------------------------+
  std::cout << "Pulling angle: " << 180 - theta_pull / M_PI * 180.0 << "°\n";

  NodesCollec NC((uword)P.nx, (uword)P.ny, P.l0, P.Ai, false, P.l0 * ((int)P.ny - 1) / 2.,
                 P.l0 * ((int)P.ny - 1) / 2., cstart, clength, with_diag);
  SpringsObj Springs; // Springs creation
  Springs.SpringsNetwork(NC.Nodes, P.EA, P.thick,
                         0); // Initial Springs state

  // +---------------------------------------------+
  // |           Load initial configuration        |
  // +---------------------------------------------+
  vec u_pre(3 * NC.Nodes.N, fill::zeros); // Initial deform
  if (geom_config) {
    u_pre.load(geom_dir, csv_ascii);
    NC.Nodes.moveNodes(u_pre);
    Springs.updateSprings(NC, P.EA, P.thick); // Initial Springs state
  }

  // +---------------------------------------------+
  // |           Find determined patterns          |
  // |               in the membrane               |
  // +---------------------------------------------+
  uvec FR = find_rectangle(NC.Nodes_del.x, cstart, cstart + clength, P.l0 * ((int)P.ny - 2) / 2.,
                           P.l0 * ((int)P.ny + 0) / 2., P.l0);
  ////FR = join_cols(FR,find_rectangle(NC.Nodes_del.x, 0.2, 0.8, 0.75, 0.85, mM.l0));

  ////uvec FR = find_pattern(NC.Nodes.x, 0.05, 0.2, M_PI_2*0.4, P.l0);
  ////uvec FRT = find_pattern(NC.Nodes.x, 0.5, 0.1, -M_PI_2*0.6, P.l0);
  ////FR = intersect(FR,FRT);
  uvec CR = find_circle_around_point(NC.Nodes_del.x, cstart, P.l0 * ((int)P.ny - 1) / 2., 0.3, P.l0);
  // +---------------------------------------------+
  // |         Increase stiffness in some          |
  // |            parts of the membrane            |
  // +---------------------------------------------+
  Springs.IncreaseStiffness_find(NC.Nodes, sort(FR), cstiff);
  // Springs.IncreaseStiffness(NC.Nodes, cstiff, P.l0 * ((int)P.ny - 1) / 2., cstart, clength);

  // +---------------------------------------------+
  // |             Modify the geometry             |
  // +---------------------------------------------+

  //// NC.create_stair(10,35);

  // +---------------------------------------------+
  // |               Create Dataframe              |
  // +---------------------------------------------+
  vec ANGLES(10000, fill::zeros);

  NC.Nodes.x.save(hdf5_name(P.dir_result, "/x", hdf5_opts::append));
  NC.Nodes.stiffer.save(hdf5_name(P.dir_result, "/stiffer", hdf5_opts::append));
  Springs.cm.save(hdf5_name(P.dir_result, "/cm", hdf5_opts::append));
  Springs.EA.save(hdf5_name(P.dir_result, "/EA", hdf5_opts::append));
  ANGLES.save(hdf5_name(P.dir_result, "/angles", hdf5_opts::append));
  // +----------------------------------------------------------+
  // | Set boundary vector, adhesion vector and traction vector |
  // +----------------------------------------------------------+

  uvec BC = find(NC.Nodes.x.col(2) > 200);
  ////uvec BC = find(abs(NC.Nodes_del.x.col(1) - P.l0 * ((int)P.ny - 1) / 2) < 0.4 * P.l0);
  /*uvec BC = find_border(NC.Nodes_del.x, P.l0, 1, true);
  BC = join_cols(BC,find_border(NC.Nodes_del.x, P.l0, 1, true));
  BC = join_cols(BC,find_border(NC.Nodes_del.x, P.l0, 0, false));
  BC = unique(BC);*/

  ////uvec DC = find_dragline_points(NC.Nodes_del.x, cstart, clength, P.l0);
  ////uvec VC = FR;
  uvec VC = find(NC.Nodes_del.x.col(0) > -1);
  // // uvec VC = find(sqrt( pow(NC.Nodes_del.x.col(0) - cstart,2) +

  uvec TC = find_pulling_points(NC.Nodes_del.x, cstart, P.l0);
  ////uvec TC = find_borders(NC.Nodes_del.x, P.l0);
  ////uvec TC = find_front_border(NC.Nodes_del.x, P.l0);

  std::cout << "TC = " << TC << "corresponging to [" << NC.Nodes.x(TC(0), 0) << ", "
            << NC.Nodes.x(TC(0), 1) << ", " << NC.Nodes.x(TC(0), 2) << "]" << std::endl;

  // +----------------------------------------------+
  // |  Defines the displacement and force vectors  |
  // +----------------------------------------------+
  vec u(3 * NC.Nodes.N, fill::zeros);        // Nodal displacement vector
  vec Du0(3 * NC.Nodes.N, fill::ones);       // Du displacement vector for arc length
  vec Qe_disp(3 * NC.Nodes.N, fill::zeros);  // External force vector for displacementcontrol
  vec Qe(3 * NC.Nodes.N, fill::zeros);       // External force vector for arclength
  vec Qi_saved(3 * NC.Nodes.N, fill::zeros); // Internal force vector

  if (arc_sign)
    Du0 = -Du0;

  // +-------------------------------------------------+
  // |  Attach nodes to substrate according to VC vec  |
  // +-------------------------------------------------+
  //* Detach the dragline
  NC.Nodes.attachNodes(VC);
  //* Detach a stiffened region
  NC.Nodes.detachNodes(FR);
  //* Detach a circle
  // NC.Nodes.detachNodes(CR);
  if (geom_config)
    NC.Nodes.detach_geom_config(u_pre);

  // +-------------------------------------------------+
  // |           Define necessary parameters           |
  // +-------------------------------------------------+
  double eps = 0.0, lambda = 0, alpha = 1;
  bool success = true, nd = false, gooddeps = true, brk = false, lownorm = true;
  bool saved_once = false;
  uword refinement = 0, refstrike = 0, no_detachments = 0;
  int final_step = 0;
  rowvec sig = zeros<rowvec>(3);

  // +-------------------------------------------------+
  // |         Set the forces direction on Qe          |
  // +-------------------------------------------------+
  pull_points(Qe, TC, theta_pull);
  // compress(Qe, TC, NC.Nodes_del.x, P.l0);
  // +---------------------------------------------------------+
  // |  Initialize or load vectors which contains the results  |
  // +---------------------------------------------------------+
  if (load_prev) {
    P.set_loaded_results(u, Springs.T, NC.Nodes.Ti, eps, sig, lambda, stp);
  } else {
    P.init_results(u, Springs.T, NC.Nodes.Ti);
    std::cout << "Result matrix initiated\n";
  }

  // +------------------------------------------+
  // |  Main loop containing solver and checks  |
  // +------------------------------------------+
  while (refinement < 20 && kl) {
    // +---------------------------------------------------+
    // |  If necessary: load state, change delta, restart  |
    // +---------------------------------------------------+
    if (!success || !gooddeps || !lownorm) { // If calculating is too hard or too easy, go back

      if (!lownorm) {
        P.arcl /= 2;
        lownorm = true;
        success = true;
        gooddeps = true;

        if (refstrike++ >= 10) {
          stp -= 1;
          u = P.u_result.col(stp);
          NC.Nodes.Ti = P.Ti_result.col(stp);
          Springs.T = P.T_result.col(stp);
          eps = P.eps_result(stp);
          sig = P.sig_result.row(stp);
          lambda = P.lambda_result(stp);
          P.arcl *= pow(2, 14);
          refstrike = 0;
        }
      } else if (!success) { // If it was too hard reduce the displacement step deps
        // evoke_exit(P, saved_once);

        stp -= 0;
        u = P.u_result.col(stp);
        NC.Nodes.Ti = P.Ti_result.col(stp);
        Springs.T = P.T_result.col(stp);
        eps = P.eps_result(stp);
        sig = P.sig_result.row(stp);
        lambda = P.lambda_result(stp);

        P.deps /= 2;
        refinement++;
        refstrike++;
        success = true;
        gooddeps = true;
        no_detachments = 0;
      } else if (!gooddeps) { // If it was too easy increase the displacement step deps
        P.deps *= 2;
        P.arcl *= 5;
        if (refinement != 0)
          refinement--;
        no_detachments = 0;
        gooddeps = true;
      }
      std::cout << "Loading from eps " << eps << "; sig: " << sig(2) << "; refstrike: " << refstrike
                << std::endl;
    }

    // +-----------------------------------+
    // |              Solver               |
    // +-----------------------------------+
    while (kl && gooddeps) {

      // +------------------------------------------------+
      // |    Run solving routine and check for success   |
      // +------------------------------------------------+
      if (solver_mode == 0) {
        // +------------------------------------------------+
        // |             Displacement control               |
        // +------------------------------------------------+
        // +------------------------------------------------+
        // |         Update imposed displacement            |
        // +------------------------------------------------+
        eps += P.deps;
        DisplacementControl(u, Qe_disp, Qi_saved, NC, Springs, u_pre, TC, BC, P, sig, eps, prestrain,
                            theta_pull, 0, success, alpha);
      } else if (solver_mode == 1) {
        // +------------------------------------------------+
        // |               Arc Length Method                |
        // +------------------------------------------------+

        ArcLength(u, Qe, Qi_saved, Du0, lambda, NC, Springs, u_pre, TC, BC, P, sig, eps, prestrain,
                  success, lownorm);
      }

      if (success == false || lownorm == false) {
        std::cout << "break at step " << stp << std::endl;
        break;
      } else {
        stp++;
        refstrike = 0;
      }

      // +------------------------------------------------+
      // |        Calculates if something broke           |
      // +------------------------------------------------+
      brk = false;
      // brk = Springs.checkBreak();

      // +------------------------------------------------+
      // | Calculates the state and tension of every node |
      // +------------------------------------------------+
      nd = true;
      nd = NC.Nodes.update_Ti_att(P, u, u_pre);
      // NC.Nodes.update_Tn_att(Qi_saved);
      uword i_T = TC(0);
      ANGLES(stp, 0) =
          atan2(NC.Nodes.x(i_T, 2) + u(3 * i_T + 2) - NC.Nodes.x(i_T + 2, 2) - u(3 * (i_T + 2) + 2),
                NC.Nodes.x(i_T, 0) + u(3 * i_T) - NC.Nodes.x(i_T + 2, 0) - u(3 * (i_T + 2)));

      // +-----------------------------------------+
      // | Decides if the calculation was too easy |
      // +-----------------------------------------+
      if (nd) {
        if (++no_detachments > 20) {
          std::cout << "increasing step" << std::endl;
          gooddeps = false;
        }
      } else
        no_detachments = 0;

      // +-------------+
      // | Set results |
      // +-------------+
      P.set_results(u, Springs.T, NC.Nodes.Ti, eps, sig, lambda, stp);

      // +--------------+
      // | Save results |
      // +--------------+
      if (signal_save) {
        signal_save = false;
        P.save_results(saved_once);
        ANGLES.save(hdf5_name(P.dir_result, "/angles", hdf5_opts::replace));
      }

      // +---------------+
      // | Increase deps |
      // +---------------+
      if (signal_USR2) {
        signal_USR2 = false;
        P.deps *= 2;
      }

      std::cout << stp << "\t" << accu(NC.Nodes.attached) / 3 / NC.Nodes.N << "\t" << sig(2) << "\t"
                << eps << std::endl;

      // +---------------------------------------------+
      // | Check if the adhesion is completed and exit |
      // +---------------------------------------------+
      if (final_step == 0) {
        if (accu(NC.Nodes.attached) <= 0 && size(VC, 0) > 2)
          final_step = stp;
      } else {
        if (stp - final_step > 0) {
          std::cout << "farewell" << endl;
          kl = false;
        }
      }
    }
  }
  // +---------------------------------------------+
  // |        Saves when exits from the loop       |
  // +---------------------------------------------+
  P.save_results(saved_once);
  ANGLES.save(hdf5_name(P.dir_result, "/angles", hdf5_opts::replace));
  std::cout << "Run ended. Time: " << P.get_timestamp() << "\n";
}
