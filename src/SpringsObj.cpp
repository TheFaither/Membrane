//
//  Springs.cpp
//  MP
//
//  Created by Daniele on 11/02/2017.
//  Copyright © 2017 DL. All rights reserved.
//

#include "SpringsObj.hpp"
#include <stdlib.h> /* srand, rand */

SpringsObj::SpringsObj() {
  cm.set_size(0, 2);
  EA.set_size(0, 1);
}

double SpringsObj::getEA(uword i, uword j) {
  // if (dl(i, j) / l0(i, j) < 0.05 || EA(i, j) < 0.05)
  if (broken(i, j) < 0.5)
    return EA(i, j);
  else
    return 0;
}

bool SpringsObj::checkBreak() {
  bool one_broke = false;

  for (uword i = 0; i < N; ++i) {
    if (dl(i) / l0(i) > 0.25 && getEA(i, 0) < 0.1 && broken(i) < 0.5) {
      one_broke = true;
      std::cout << i << " has broken. EA = " << EA(i) << "\n";
      broken(i, 0) = 1;
    }
  }

  return one_broke;
}

void SpringsObj::updateSprings(NodesCollec NC, double E, double h) {
  for (uword i = 0; i < N; ++i) {
    l0(i) = sqrt(pow(NC.Nodes.x(cm(i, 0), 0) - NC.Nodes.x(cm(i, 1), 0), 2) +
                 pow(NC.Nodes.x(cm(i, 0), 1) - NC.Nodes.x(cm(i, 1), 1), 2));
  }

  for (int j = 0; j < N; j++) {
    if (NC.Nodes_del.x(cm(j, 0), 0) == NC.Nodes_del.x(cm(j, 1), 0)) {
      EA(j) = 2. * 3. / 8. * E * h * l0(j, 0);
    } else if (NC.Nodes_del.x(cm(j, 0), 1) == NC.Nodes_del.x(cm(j, 1), 1)) {
      EA(j) = 2. * 3. / 8. * E * h * l0(j, 0);
    } else {
      EA(j) = 1. * 3. / 8. * E * h * l0(j, 0);
    }
  }
}

void SpringsObj::SpringsNetwork(NodesObj Nodes, double E, double h, bool hex) {
  std::cout << "Building Springs" << std::endl;

  cm.set_size(Nodes.sup.n_rows * Nodes.sup.n_rows, 2);

  uword c = 0;

  for (uword i = 0; i < Nodes.sup.n_rows; i++) {
    for (uword j = 1; j < Nodes.sup.n_cols; j++) {
      if (Nodes.sup(i, j) != 0) {
        urowvec couple = {Nodes.sup(i, 0) - 1, Nodes.sup(i, j) - 1};
        cm.row(c++) = sort(couple);
      }
    }
  }
  cm = delete_zeros(cm);
  cm = unique_rows(cm);
  N = cm.n_rows;

  l.set_size(N);
  l0.set_size(N);
  for (uword i = 0; i < N; ++i) {
    l0(i) = sqrt(pow(Nodes.x(cm(i, 0), 0) - Nodes.x(cm(i, 1), 0), 2) +
                 pow(Nodes.x(cm(i, 0), 1) - Nodes.x(cm(i, 1), 1), 2));
  }
  l = l0;
  dl = zeros(N, 1);
  EA.set_size(N, 1);
  T.set_size(N, 1);
  broken.set_size(N, 1);
  broken.fill(0);

  if (hex == 0) {
    rowvec maxx = max(Nodes.x);
    std::cout << maxx << std::endl;

    for (int j = 0; j < N; j++) {
      double x1, x2, y1, y2;
      x1 = Nodes.x(cm(j, 0), 0);
      x2 = Nodes.x(cm(j, 1), 0);
      y1 = Nodes.x(cm(j, 0), 1);
      y2 = Nodes.x(cm(j, 1), 1);

      if (std::abs(x1 - x2) < 1E-2 * Nodes.l0 && std::abs(x1) > 1E-1 * Nodes.l0 &&
          std::abs(x1 - maxx(0)) > 1E-1 * Nodes.l0) {
        EA(j) = 2. * 3. / 8. * E * h * l0(j, 0);
      } else if (std::abs(y1 - y2) < 1E-2 * Nodes.l0 && std::abs(y1) > 1E-1 * Nodes.l0 &&
                 std::abs(y1 - maxx(1)) > 1E-1 * Nodes.l0) {
        EA(j) = 2. * 3. / 8. * E * h * l0(j, 0);
      } else {
        EA(j) = 1. * 3. / 8. * E * h * l0(j, 0);
      }

      //            if (fabs(l(j) - l_min) < 1E-6)
      //                EA(j) = 2.*k_v;
      //            else
      //                EA(j) = k_v;
    }
  }
}

void SpringsObj::RandomizeStiffness() {
  EA.for_each([](mat::elem_type &val) { val += rand() % 100 / (10 * val); });
}

void SpringsObj::IncreaseStiffness(NodesObj &Nodes, double coefficient, double ycondition, double xstart,
                                   double clength) {
  uvec co;
  co.set_size(N);
  co.zeros();
  uword c = 0;
  for (uword i = 0; i < N; i++) {
    if (Nodes.x(cm(i, 0), 1) == Nodes.x(cm(i, 1), 1) &&
        std::abs(Nodes.x(cm(i, 1), 1) - ycondition) <= l0(i, 0) * 4.9E-1 &&
        Nodes.x(cm(i, 0), 0) >= xstart - l0(i, 0) * 1E-1 &&
        Nodes.x(cm(i, 0), 0) <= xstart + clength + l0(i, 0) * 1E-1) {
      Nodes.stiffer(cm(i, 0)) = 1;
      Nodes.stiffer(cm(i, 1)) = 1;
      co(c++) = i;
    }
  }

  for (uword j = 0; j < co.n_elem; ++j) {
    if (co(j) == 0)
      break;
    else
      EA(co(j)) *= coefficient;
  }
}

void SpringsObj::IncreaseStiffness_find(NodesObj &Nodes, uvec FR, double coefficient) {
  uvec co;
  co.set_size(N);
  co.zeros();
  uword c = 0;
  for (uword i = 0; i < N; i++) {
    if (any(abs(conv_to<vec>::from(FR) - cm(i, 0)) < 0.5) &&
        any(abs(conv_to<vec>::from(FR) - cm(i, 1)) < 0.5)) {
      co(c++) = i;
      Nodes.stiffer(cm(i, 0)) = 1;
      Nodes.stiffer(cm(i, 1)) = 1;
      ////std::cout << cm(i,0) << " " << cm(i,1) << " \n";
    }
  }
  for (uword j = 0; j < co.n_elem; ++j) {
    if (co(j) == 0 && j != 0)
      break;
    else
      EA(co(j)) *= coefficient;
  }
}