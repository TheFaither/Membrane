//
//  DisplacementControl.cpp
//  MP
//
//  Created by Daniele on 12/02/2017.
//  Copyright © 2017 DL. All rights reserved.
//

#include "Equilibrium.hpp"

void DisplacementControl(vec &u, vec &Qe, vec &Qi_saved, NodesCollec &NC, SpringsObj &Springs, mat u_pre,
                         uvec TC, uvec BC, const Params &prms, rowvec &sig, double &eps,
                         double &prestrain, double &theta, double phi, bool &success,
                         double givenalpha) {

  double ALPHA = givenalpha;
  superlu_opts settings;
  settings.permutation = superlu_opts::COLAMD;
  settings.refine = superlu_opts::REF_NONE;
  settings.symmetric = false;
  settings.equilibrate = false;

  vec Qi(size(Qe), fill::zeros);

  double lastnrm = 1000.0;
  double nrm = 0.0;
  int itmax = 300;

  sp_mat K_sp;
  sp_mat K_sp_i;
  sp_mat K_sp_c;
  for (int it = 0;; it++) {
    if (it % 10 == 0 && it > 0) {
      nrm = norm(Qe - Qi);
      if (it % 100 == 0)
        std::cout << norm(Qe - Qi) << std::endl;
      if (it > 20 && (lastnrm < nrm || std::abs(lastnrm - nrm) < 1.0E-11) && ALPHA > 300) {
        ALPHA /= 2;
        std::cout << "at " << it << " with ALPHA " << ALPHA << std::endl;
      }
      lastnrm = nrm;
    }

    Qi.zeros();

    for (uword iq = 0; iq < size(TC, 0); ++iq) {
      Qe(3 * TC(iq) + 0) = PENALTY * (eps * cos(theta) * cos(phi) - u(3 * TC(iq) + 0));
      Qe(3 * TC(iq) + 1) = 0.0;
      Qe(3 * TC(iq) + 2) = PENALTY * (eps * sin(theta) - u(3 * TC(iq) + 2));
    }

    Qe = setnulls(Qe);

    K_sp = sys_eq(u, Qi, NC, Springs, prestrain, u_pre);
    K_sp_i = interface_lin(u, Qi, NC, Springs, TC, BC, prestrain, u_pre, prms);

    sig.zeros();
    for (uword iq = 0; iq < size(TC, 0); ++iq) {
      double tempsig = 0.0;
      tempsig += Qi.at(3 * TC(iq) + 0) * Qi.at(3 * TC(iq) + 0);
      tempsig += Qi.at(3 * TC(iq) + 1) * Qi.at(3 * TC(iq) + 1);

      sig(0) += sqrt(tempsig);
      sig(1) += fabs(Qi.at(3 * TC(iq) + 2));
      sig(2) += sqrt(tempsig + Qi.at(3 * TC(iq) + 2) * Qi.at(3 * TC(iq) + 2));
    }

    K_sp_c = K_constraints(NC, Springs, TC, BC);
    Qi_constraints(Qi, TC, BC);

    try {
      u -= ALPHA * spsolve(K_sp + K_sp_i + K_sp_c, (Qi - Qe), "superlu", settings);
    } catch (...) {
      success = false;
      break;
      // ALPHA/=2;
    }

    nrm = norm(Qe - Qi);
    // std::cout << nrm << std::endl;

    if (nrm < 1E-10)
      break;

    if (it >= itmax) {
      std::cout << "max iterations" << endl;
      success = false;
      break;
    }

  } // end for iterations
}
