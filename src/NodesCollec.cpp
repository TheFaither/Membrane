//
//  NodesCollec.cpp
//  MP
//
//  Created by Daniele on 12/02/2017.
//  Copyright © 2017 DL. All rights reserved.
//

#include "NodesCollec.hpp"

NodesCollec::NodesCollec(uword nx, uword ny, double l0, double Ai, bool circular, double yd1, double yd2,
                         double xd, double xl, bool wd) {
  try {
    Nodes = *new NodesObj(nx, ny, l0, Ai, circular, yd1, yd2, xd, xl, wd);
  } catch (...) {
    std::cerr << "Failed to create Nodes\n";
    exit(1);
  }
  std::cout << "Nodes created\n";

  try {
    Nodes_del = *new NodesObj;
    Nodes_del.copynodes(Nodes);
  } catch (...) {
    std::cerr << "Failed to create Nodes_del\n";
    exit(1);
  }
  std::cout << "Nodes_del created\n";
}

void NodesCollec::create_stair(uword c1, uword c2) {
  uword nx = Nodes.nx;
  uword ny = Nodes.ny;
  double xlim1 = Nodes_del.x(c1, 0);
  double xlim2 = Nodes_del.x(c2, 0);

  for (uword j = 0; j < ny; j++) {
    for (uword i = 0; i < nx; i++) {
      {
        if (i >= c1 && i < c2) {
          Nodes.x(j * nx + i, 0) = xlim1;
          Nodes.x(j * nx + i, 2) = Nodes_del.x(j * nx + i, 0) - xlim1;
        }
        if (i >= c2) {
          Nodes.x(j * nx + i, 0) = Nodes_del.x(j * nx + i, 0) - (xlim2 - xlim1);
          Nodes.x(j * nx + i, 2) = (xlim2 - xlim1);
        }
      }
    }
  }

  std::cout << "Stair created\n";
}