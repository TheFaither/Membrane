//
//  ArcLength.cpp
//  MP
//
//  Created by Daniele on 12/03/2019.
//  Copyright © 2019 DL. All rights reserved.
//

#include "Equilibrium.hpp"

void ArcLength(vec &u, vec &Qe, vec &Qi_saved, vec &Du0, double &lambda, NodesCollec &NC,
               SpringsObj &Springs, vec u_pre, uvec TC, uvec BC, const Params &prms, rowvec &sig,
               double &eps, double &prestrain, bool &success, bool &lownorm) {

  double ARCL = prms.arcl;

  superlu_opts settings;
  settings.permutation = superlu_opts::COLAMD;
  settings.refine = superlu_opts::REF_NONE;
  settings.symmetric = false;
  settings.equilibrate = false;

  vec Qi(size(Qe), fill::zeros);
  vec Du(u);
  vec Dun(u);
  vec DQe(u);
  vec du_star(u);
  vec du_bar(u);
  vec du(u);
  vec Res(Qe);

  double Dlambda = 0;
  double dlambda = 0;
  double nrm = 200.0;
  int itmax = 1000;

  // +---------------------------------------------*
  // |                   PREDICTOR                 |
  // +---------------------------------------------*
  sp_mat K_sparse = sys_eq(u, NC, Springs, prestrain, u_pre, prms);

  try {
    du_bar = spsolve(K_sparse, Qe, "superlu", settings);
  } catch (...) {
    success = false;
    // exit(2);
  }
  if (dot(Du0, du_bar) > 0)
    Dlambda = ARCL / sqrt(dot(du_bar, du_bar));
  else
    Dlambda = -ARCL / sqrt(dot(du_bar, du_bar));

  lambda += Dlambda;
  DQe = Dlambda * Qe;

  try {
    Du = spsolve(K_sparse, DQe, "superlu", settings);
  } catch (...) {
    success = false;
    // exit(2);
  }

  u += Du;

  build_Qi(u, Qi, NC, Springs, prestrain, u_pre, prms);

  Res = Qi - lambda * Qe;

  // +---------------------------------------------*
  // |                   CORRECTOR                 |
  // +---------------------------------------------*
  vec uc(u);
  du.zeros();
  uc = u + du;
  dlambda = 0;
  Dun = Du;

  for (int it = 0; it < 1; it++) {

    // +---------------------------------------------*
    // |      Printing the norm every 100 steps      |
    // +---------------------------------------------*
    if (it % 100 == 0 && it > 0) {
      nrm = norm(Res) / norm(Qe);
      std::cout << nrm << std::endl;
      // if (it > 150 && std::abs(lastnrm - nrm) < 1.0E-13)
    }
    // +---------------------------------------------*
    // |                   Build K                   |
    // +---------------------------------------------*

    K_sparse = sys_eq(uc, NC, Springs, prestrain, u_pre, prms);

    // +---------------------------------------------*
    // |              Tangent vectors                |
    // +---------------------------------------------*
    try {
      du_star = -spsolve(K_sparse, Res, "superlu", settings);
      du_bar = spsolve(K_sparse, Qe, "superlu", settings);

      // +---------------------------------------------*
      // |                Riks or Ramm                 |
      // +---------------------------------------------*
      // Ramm
      Du = Dun + du;
      // Riks
      // Du = Dun;

      // +---------------------------------------------*
      // |                Find dlambda                 |
      // +---------------------------------------------*
      double dlambda_1 = -dot(Du, du_star) / dot(Du, du_bar);

      dlambda += dlambda_1;
      du += du_star + dlambda_1 * du_bar;

      uc = u + du;
    } catch (...) {
      success = false;
      // exit(3);
    }

    // +---------------------------------------------*
    // |            Update Internal forces           |
    // +---------------------------------------------*

    build_Qi(uc, Qi, NC, Springs, prestrain, u_pre, prms);

    // +---------------------------------------------*
    // |                 Residual                    |
    // +---------------------------------------------*

    Res = Qi - (lambda + dlambda) * Qe;

    nrm = norm(Res) / norm(Qe);

    if (nrm > 1E-3) {
      lownorm = false;
      std::cout << nrm << std::endl;
    } else
      lownorm = true;

    if (it >= itmax) {
      std::cout << "max iterations" << endl;
      success = false;
      break;
    }

    // +---------------------------------------------*
    // |                 Last Update                 |
    // +---------------------------------------------*

    u = u + du;
    Du0 = Dun + du;
    lambda = lambda + dlambda;
    sig.zeros();
    eps = 0.0;

    for (uword iq = 0; iq < size(TC, 0); ++iq) {
      double tempsig = 0.0;
      double tempeps = 0.0;
      tempsig += Qi.at(3 * TC(iq) + 0) * Qi.at(3 * TC(iq) + 0);
      tempsig += Qi.at(3 * TC(iq) + 1) * Qi.at(3 * TC(iq) + 1);
      tempeps += u.at(3 * TC(iq) + 0) * u.at(3 * TC(iq) + 0);
      tempeps += u.at(3 * TC(iq) + 1) * u.at(3 * TC(iq) + 1);
      tempeps += u.at(3 * TC(iq) + 2) * u.at(3 * TC(iq) + 2);

      sig(0) += sqrt(tempsig);
      sig(1) += fabs(Qi.at(3 * TC(iq) + 2));
      sig(2) += sqrt(tempsig + Qi.at(3 * TC(iq) + 2) * Qi.at(3 * TC(iq) + 2));
      eps += sqrt(tempeps);
    }

    eps /= size(TC, 0);
    Qi_saved = Qi;
  } // end for iterations
}

// ***********************************************
// /\                                           \/
// /\                  CREATE K                 \/
// /\                                           \/
// ***********************************************
sp_mat sys_eq(vec &u, NodesCollec &NC, SpringsObj &Springs, const double &prestrain, vec u_pre,
              const Params &prms) {

  double umaxI = prms.umaxI;
  double umaxII = prms.umaxII;
  double kiI = prms.kiI;
  double kiII = prms.kiII;

  const double n3d = 36;
  int limit = Springs.N;

  // Initialisation of the stiffness matrix using sparse formulation
  umat locations(2 * n3d * Springs.N, 2);
  mat Kc(2 * n3d * Springs.N, 1);
  locations.zeros();
  Kc.zeros();

  mat diagonal = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

  mat klm(3, 3);
  mat klg(3, 3);
  mat n_sqr(3, 3);
  for (int j = 0; j < limit; j++) {
    //    if (Springs.broken(j) < 0.5) {
    uword k = Springs.cm(j, 0); // 1st node of the spring
    uword l = Springs.cm(j, 1); // 2nd node of the spring

    umat sup = {3 * k, 3 * k + 1, 3 * k + 2,
                3 * l, 3 * l + 1, 3 * l + 2}; // Vector of spring coordinates
    sup = sup.t();

    Springs.l.at(j, 0) =
        sqrt(pow((NC.Nodes.x(k, 0) + u(3 * k) - NC.Nodes.x(l, 0) - u(3 * l)), 2) +
             pow((NC.Nodes.x(k, 1) + u(3 * k + 1) - NC.Nodes.x(l, 1) - u(3 * l + 1)), 2) +
             pow((NC.Nodes.x(k, 2) + u(3 * k + 2) - NC.Nodes.x(l, 2) - u(3 * l + 2)), 2));

    Springs.dl.at(j, 0) =
        (Springs.l.at(j, 0) * Springs.l.at(j, 0) - (Springs.l0.at(j, 0) * Springs.l0.at(j, 0))) /
        (Springs.l.at(j, 0) + Springs.l0.at(j, 0));

    // Compute the local material and geometric stiffness matrix
    double n1 =
        (NC.Nodes.x.at(l, 0) + u.at(3 * l) - NC.Nodes.x.at(k, 0) - u.at(3 * k)) / Springs.l.at(j, 0);
    double n2 = (NC.Nodes.x.at(l, 1) + u.at(3 * l + 1) - NC.Nodes.x.at(k, 1) - u.at(3 * k + 1)) /
                Springs.l.at(j, 0);
    double n3 = (NC.Nodes.x.at(l, 2) + u.at(3 * l + 2) - NC.Nodes.x.at(k, 2) - u.at(3 * k + 2)) /
                Springs.l.at(j, 0);

    n_sqr.zeros();
    n_sqr = {{n1 * n1, n1 * n2, n1 * n3}, {n1 * n2, n2 * n2, n2 * n3}, {n1 * n3, n2 * n3, n3 * n3}};
    mat n_n = {-n1, -n2, -n3, n1, n2, n3};

    Springs.T(j, 0) = Springs.getEA(j, 0) / Springs.l0(j, 0) * Springs.dl(j, 0);
    double T_Prestrain = Springs.getEA(j, 0) * prestrain;

    klm.zeros();
    klg.zeros();
    klm = Springs.getEA(j, 0) / Springs.l0(j, 0) * n_sqr;
    klg = (Springs.T(j, 0) + T_Prestrain) / Springs.l(j, 0) * diagonal;

    mat km(klm);
    km = join_horiz(km, -km);
    km = join_vert(km, -km);

    mat kg(klg);
    kg = join_horiz(kg, -kg);
    kg = join_vert(kg, -kg);

    // Assembly in the global stiffness matrix
    locations(span(j * n3d, (j + 1) * n3d - 1), 0) = repmat(sup, 6, 1);
    locations(span(j * n3d, (j + 1) * n3d - 1), 1) =
        sort(locations(span(j * n3d, (j + 1) * n3d - 1), 0));
    Kc.rows(j * n3d, (j + 1) * n3d - 1) = reshape(km, n3d, 1);

    locations(span(n3d * Springs.N + j * n3d, n3d * Springs.N + (j + 1) * n3d - 1), 0) =
        repmat(sup, 6, 1);
    locations(span(n3d * Springs.N + j * n3d, n3d * Springs.N + (j + 1) * n3d - 1), 1) =
        sort(locations(span(n3d * Springs.N + j * n3d, n3d * Springs.N + (j + 1) * n3d - 1), 0));
    Kc.rows(n3d * Springs.N + j * n3d, n3d * Springs.N + (j + 1) * n3d - 1) = reshape(kg, n3d, 1);

    //    } else
    //      Springs.T(j) = 0;
  } // end springs for

  // Applying interface contribuition
  umat InterfLoc(9 * NC.Nodes.N, 2);
  InterfLoc.fill(0);
  urowvec couple(2);
  mat InterfKc = zeros(9 * NC.Nodes.N, 1);
  for (uword i = 0; i < NC.Nodes.N; ++i) {
    for (uword ib = 0; ib < 3; ++ib) {
      for (uword ia = 0; ia < 3; ++ia) {

        couple << 3 * i + ia << 3 * i + ib;
        InterfLoc.row(9 * i + ia + 3 * ib) = couple;
        if (ia == ib) {
          if (NC.Nodes.init_att(3 * i, 0) == 0)
            InterfKc(9 * i + ia + 3 * ib, 0) =
                NC.Nodes.dExpTd(kiI * 1E-3, kiII * 1E-3, umaxI, umaxII, u, i, ib);
          else
            InterfKc(9 * i + ia + 3 * ib, 0) = NC.Nodes.dExpTd(kiI, kiII, umaxI, umaxII, u, i, ib);
        } else {
          if (NC.Nodes.init_att(3 * i, 0) == 0)
            InterfKc(9 * i + ia + 3 * ib, 0) =
                NC.Nodes.dExpTm(kiI * 1E-3, kiII * 1E-3, umaxI, umaxII, u, i, ib, ia);
          else
            InterfKc(9 * i + ia + 3 * ib, 0) = NC.Nodes.dExpTm(kiI, kiII, umaxI, umaxII, u, i, ib, ia);
        }
      }
    }
  }

  // std::cout << "Kc: " << sp_mat(InterfKc) << " LOC: " << sp_umat(InterfLoc) << std::endl;
  locations = join_vert(locations, InterfLoc);
  Kc = join_vert(Kc, InterfKc);
  // apply_interface(NC, u + u_pre, prms, locations, Kc);

  locations = locations.t();
  return sp_mat(true, locations, Kc, 3 * NC.Nodes.N, 3 * NC.Nodes.N, true, true);
}

// +---------------------------------------------*
// |                                             |
// |                   CREATE Qi                 |
// |                                             |
// +---------------------------------------------*
void build_Qi(vec &u, vec &Qi, NodesCollec &NC, SpringsObj &Springs, const double &prestrain, vec u_pre,
              const Params &prms) {

  double umaxI = prms.umaxI;
  double umaxII = prms.umaxII;
  double kiI = prms.kiI;
  double kiII = prms.kiII;

  Qi.zeros();
  const double n3d = 36;
  int limit = Springs.N;

  // Initialisation of the stiffness matrix using sparse formulation
  umat locations(2 * n3d * Springs.N, 2);
  mat Kc(2 * n3d * Springs.N, 1);
  locations.zeros();
  Kc.zeros();

  mat diagonal = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

  for (int j = 0; j < limit; j++) {
    //  if (Springs.broken(j) < 0.5) {
    uword k = Springs.cm(j, 0); // 1st node of the spring
    uword l = Springs.cm(j, 1); // 2nd node of the spring

    umat sup = {3 * k, 3 * k + 1, 3 * k + 2,
                3 * l, 3 * l + 1, 3 * l + 2}; // Vector of spring coordinates
    sup = sup.t();

    Springs.l.at(j, 0) =
        sqrt(pow((NC.Nodes.x(k, 0) + u(3 * k) - NC.Nodes.x(l, 0) - u(3 * l)), 2) +
             pow((NC.Nodes.x(k, 1) + u(3 * k + 1) - NC.Nodes.x(l, 1) - u(3 * l + 1)), 2) +
             pow((NC.Nodes.x(k, 2) + u(3 * k + 2) - NC.Nodes.x(l, 2) - u(3 * l + 2)), 2));

    Springs.dl.at(j, 0) =
        (Springs.l.at(j, 0) * Springs.l.at(j, 0) - (Springs.l0.at(j, 0) * Springs.l0.at(j, 0))) /
        (Springs.l.at(j, 0) + Springs.l0.at(j, 0));

    // Compute the local material and geometric stiffness matrix
    double n1 =
        (NC.Nodes.x.at(l, 0) + u.at(3 * l) - NC.Nodes.x.at(k, 0) - u.at(3 * k)) / Springs.l.at(j, 0);
    double n2 = (NC.Nodes.x.at(l, 1) + u.at(3 * l + 1) - NC.Nodes.x.at(k, 1) - u.at(3 * k + 1)) /
                Springs.l.at(j, 0);
    double n3 = (NC.Nodes.x.at(l, 2) + u.at(3 * l + 2) - NC.Nodes.x.at(k, 2) - u.at(3 * k + 2)) /
                Springs.l.at(j, 0);

    mat n_sqr = {{n1 * n1, n1 * n2, n1 * n3}, {n1 * n2, n2 * n2, n2 * n3}, {n1 * n3, n2 * n3, n3 * n3}};
    mat n_n = {-n1, -n2, -n3, n1, n2, n3};

    Springs.T(j, 0) = Springs.getEA(j, 0) / Springs.l0(j, 0) * Springs.dl(j, 0);
    double T_Prestrain = Springs.getEA(j, 0) * prestrain;

    // Update internal forces
    mat Qi_mat = (Springs.T(j, 0) + T_Prestrain) * n_n;
    for (uword i = 0; i < sup.n_elem; ++i) {
      Qi(sup(i)) += Qi_mat(0, i);
    }
    //  } else {
    //    Springs.T(j) = 0;
    //}

  } // end springs for

  for (uword var = 0; var < Qi.n_rows; ++var) {
    if (NC.Nodes.init_att(var, 0) == 0)
      Qi(var) += NC.Nodes.ExpT(kiI * 1E-3, kiII * 1E-3, umaxI, umaxII, u + u_pre, var);
    else
      Qi(var) += NC.Nodes.ExpT(kiI, kiII, umaxI, umaxII, u + u_pre, var);
  }
}