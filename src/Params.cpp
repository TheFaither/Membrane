#include "Params.hpp"

std::string Params::get_timestamp() {
  auto now = std::time(nullptr);
  char buf[sizeof("YYYY-MM-DD  HH-MM-SS:")];
  return std::string(buf, buf + std::strftime(buf, sizeof(buf), "%F--%H-%M-%S", std::gmtime(&now)));
}

std::string Params::get_save_dir() {
  int c = 1;
  std::string dir_result = "Output/Result" + get_timestamp() + "_1";
  std::string extension = ".hdf";
  while (exists_file(dir_result) || exists_file(dir_result + extension)) {
    if (c <= 9)
      dir_result.replace(dir_result.end() - 1, dir_result.end(), std::to_string(++c));
    else
      dir_result.replace(dir_result.end() - 2, dir_result.end(), std::to_string(++c));
  }
  return dir_result + extension;
}

std::string Params::get_save_dir(std::string ID) {

  std::string dir_result = "Output/Result" + get_timestamp();
  std::string extension = ".hdf";
  std::string IDcode = "_" + ID;
  return dir_result + IDcode + extension;
}

Params::Params(long n_input, std::string ID, std::string exe_dir) {

  this->ID = ID;

  // dir_result = "Output/Result" + get_timestamp() + ".hdf";
  if (ID.empty())
    dir_result = get_save_dir();
  else
    dir_result = get_save_dir(ID);

  this->exe_dir = exe_dir;
  ic = input_cond();
  arcl = arcl_cond();

  set_params(n_input);

  ic.save(hdf5_name(dir_result, "/inputs"));
}

void Params::set_params(long n_input) {

  int j = 0;

  opt_alpha = NAN;
  opt_cd = NAN;
  opt_cl = NAN;

  sidex = ic(j++);
  sidey = ic(j++);
  double area = sidex * sidey;
  if (sidey < 0.01) {
    nx = (double)n_input * n_input;
    ny = 1;
  } else {
    nx = (double)n_input * sidex / sqrt(area) + 1.1,
    ny = (double)n_input * sidey / sqrt(area); // Nodes in x and y direction
    ny = floor(ny) - fmod(floor(ny), 2);
    ny += 1.1;
  }

  l0 = sidex / (int)(nx - 1.); // nodes spacing
  GI = ic(j++);     // Energy release rate. Must be divided by two to obtain the physical result
  GII = ic(j++);    // Energy release rate
  umaxI = ic(j++);  // Spring failure strain
  umaxII = ic(j++); // Spring failure strain
  thick = ic(j++);  // cell thickness

  Ai = l0 * l0;                     // interface effective area = w * l
  Tmax = 2 * GI * Ai / umaxI;       // interface stiffness
  kiI = 2 * GI / umaxI / umaxI;     // interface effective area = w * l
  kiII = 2 * GII / umaxII / umaxII; // interface effective area = w * l

  EA = ic(j++); // Elastic modulus

  deps = ic(j++); // Increment

  if (ic.n_elem > 9)
    opt_alpha = ic(j++);
  if (ic.n_elem > 10)
    opt_cd = ic(j++);
  if (ic.n_elem > 11)
    opt_cl = ic(j++);
  if (ic.n_elem > 12)
    opt_cE = ic(j++);

  ic.resize(ic.n_elem + 6);

  ic(j++) = Ai;
  ic(j++) = kiI;
  ic(j++) = kiII;
  ic(j++) = nx;
  ic(j++) = ny;
  ic(j++) = l0;
}

mat Params::input_cond() {
  mat inputs;
  std::string input_dir = exe_dir + "Input/input.csv";
  inputs.load(input_dir, csv_ascii);
  return inputs;
}

double Params::arcl_cond() {
  mat arc_m;
  arc_m.load("Input/arcl.csv", csv_ascii);
  double arcl = arc_m(0, 0);
  return arcl;
}

void Params::init_results(const vec &u, const vec &T, const vec &Ti) {
  u_result.set_size(u.n_elem, 20);
  T_result.set_size(T.n_elem, 20);
  Ti_result.set_size(Ti.n_elem, 20);
  eps_result.zeros(20);
  sig_result.zeros(20, 3);
  lambda_result.zeros(20);
}

void Params::set_results(const vec &u, const vec &T, const vec &Ti, double eps, rowvec sig,
                         double lambda, uword stp) {
  try {
    try {
      u_result.col(stp) = u;
      T_result.col(stp) = T;
      Ti_result.col(stp) = Ti;

      eps_result(stp) = eps;
      sig_result.row(stp) = sig;
      lambda_result(stp) = lambda;
    } catch (...) {
      std::cerr << "Impossible to fill result. Expanding the Results\n";
      expand_results();
      u_result.col(stp) = u;
      T_result.col(stp) = T;
      Ti_result.col(stp) = Ti;

      eps_result(stp) = eps;
      sig_result.row(stp) = sig;
      lambda_result(stp) = lambda;
    }
  } catch (...) {
    std::cerr << "Impossible to fill result";
  }
}

void Params::expand_results() {
  u_result.resize(u_result.n_rows, u_result.n_cols + 20);
  T_result.resize(T_result.n_rows, T_result.n_cols + 20);
  Ti_result.resize(Ti_result.n_rows, Ti_result.n_cols + 20);
  eps_result.resize(eps_result.n_elem + 20, 1);
  sig_result.resize(sig_result.n_rows + 20, 3);
  lambda_result.resize(lambda_result.n_elem + 20, 1);
}

void Params::save_results(bool &so) {
  if (!so) {
    so = true;
    try {
      u_result.save(hdf5_name(dir_result, "/u", hdf5_opts::append));
      T_result.save(hdf5_name(dir_result, "/T", hdf5_opts::append));
      Ti_result.save(hdf5_name(dir_result, "/Ti", hdf5_opts::append));
      eps_result.save(hdf5_name(dir_result, "/eps", hdf5_opts::append));
      sig_result.save(hdf5_name(dir_result, "/sig", hdf5_opts::append));
      lambda_result.save(hdf5_name(dir_result, "/lambda", hdf5_opts::append));
    } catch (...) {
      std::cerr << "unable to save\n";
    }
  } else
    try {
      u_result.save(hdf5_name(dir_result, "/u", hdf5_opts::replace));
      T_result.save(hdf5_name(dir_result, "/T", hdf5_opts::replace));
      Ti_result.save(hdf5_name(dir_result, "/Ti", hdf5_opts::replace));
      eps_result.save(hdf5_name(dir_result, "/eps", hdf5_opts::replace));
      sig_result.save(hdf5_name(dir_result, "/sig", hdf5_opts::replace));
      lambda_result.save(hdf5_name(dir_result, "/lambda", hdf5_opts::replace));
    } catch (...) {
      std::cerr << "unable to save\n";
    }
}

void Params::load_results() {
  try {
    std::string dir_load = "./in_config.hdf";
    u_result.load(hdf5_name(dir_load, "u"));
    T_result.load(hdf5_name(dir_load, "T"));
    Ti_result.load(hdf5_name(dir_load, "Ti"));
    eps_result.load(hdf5_name(dir_load, "eps"));
    sig_result.load(hdf5_name(dir_load, "sig"));
    lambda_result.load(hdf5_name(dir_load, "lambda"));
    ic.load(hdf5_name(dir_load, "inputs"));
    std::cerr << "loaded\n";

  } catch (...) {
    std::cerr << "unable to load\n";
  }
}

void Params::deform(double r) {
  double n = (ny - 1.1) / r;
  ny = n + 1.1, nx = n * r * r + 1.1; // Nodes in x and y dir_resultection
  ny -= (int)ny % 2 - 0.1;
  std::cout << nx << " " << ny << std::endl;
}

void Params::print_run_info() {
  std::cout << "Membrane by Daniele Liprandi"
            << ".\n"
            << "TIME: " << get_timestamp() << ".\n"
            << "Membrane of size [" << sidex << ", " << sidey << "] mm\n"
            << "Number of nodes: " << nx << ", " << ny << ".\n"
            << "Adhesive energy: [" << GI << ", " << GII << "] GPa mm.\n"
            << "Young Modulus:" << EA << " MPa.\n"
            << "Arc length: " << arcl << ".\n";
}

void Params::set_loaded_results(vec &u, vec &T, vec &Ti, double &eps, rowvec &sig, double &lambda,
                                uword stp) {
  u = u_result.col(stp);
  Ti = Ti_result.col(stp);
  T = T_result.col(stp);
  eps = eps_result(stp);
  sig = sig_result.row(stp);
  lambda = lambda_result(stp);

  std::cout << "parameters set to current step\n";
  std::cout << "Loading from eps " << eps << "; lambda: " << lambda << "; sig: " << sig(2) << std::endl;
}
