//
//  NodesObj.cpp
//  MP
//
//  Created by Daniele on 11/02/2017.
//  Copyright © 2017 DL. All rights reserved.
//

#include "NodesObj.hpp"

NodesObj::NodesObj() {}

NodesObj::NodesObj(uword nx, uword ny, double l0, double Ai, bool circular, double yd1, double yd2,
                   double xd, double xl, bool with_diag) {
  std::cout << "Building Nodes" << std::endl;

  this->l0 = l0;
  this->nx = nx;
  this->ny = ny;
  N = nx * ny;
  this->Ai.set_size(N, 1);
  this->Ai.fill(Ai);

  x = zeros(N, 3);

  for (uword j = 0; j < ny; j++) {
    for (uword i = 0; i < nx; i++) {
      // if ((i-0.5*nx)*(i-0.5*nx)+(j-0.5*ny)*(j-0.5*ny) < 0.5*0.5*nx*ny)
      {
        x(j * nx + i, 0) = l0 * i;
        x(j * nx + i, 1) = l0 * j;
        x(j * nx + i, 2) = 0; // grid on xy-plane
        if (i == 0 || i == nx - 1) {
          this->Ai(j * nx + i, 0) /= 2;
        }

        if (j == 0 || j == ny - 1) {
          this->Ai(j * nx + i, 0) /= 2;
        }
      }
      /* else
      {
          x(j*nx + i,0) = -1;
          x(j*nx + i,1) = -1;
          x(j*nx + i,2) = -1;
      }
      */
    }
  }

  x = delete_minus(x);
  N = x.n_rows;

  sup.set_size(N, 9);
  Ti.set_size(N, 1);
  Tn.set_size(N, 1);
  stiffer.set_size(N, 1);
  stiffer.fill(0);
  attached.set_size(3 * N, 1);
  attached.fill(1);
  init_att.set_size(3 * N, 1);

  for (int i = 0; i < N; ++i) {
    if (x(i, 1) >= yd1 && x(i, 1) <= yd2 && x(i, 0) >= xd && x(i, 0) <= xd + xl) {
      // std::cout << "Dragline at = " << i << "corresponging to [" << x(i,0) << ", " << x(i,1) << ", "
      // << x(i,2) << "]"<< std::endl;
      set_init_att(i, 0);
    } else {
      set_init_att(i, 1);
    }
  }

  // support generation
  /* The relative distance is calculated from all the points to eachothers; a vector of index is then
   + created, with the center as the first number and the first neighbours following; all the vectors are
   + then joint together in the sup matrix. The cm (connections matrix) matrix of Springs is hardly
   + connected to the sup matrix.
   */
  std::cout << "Building support" << std::endl;

  for (uword i = 0; i < N; ++i) {
    mat distance;
    distance.set_size(N, 1);
    for (int var = 0; var < N; ++var) {
      distance(var) = sqrt((x(var, 0) - x(i, 0)) * (x(var, 0) - x(i, 0)) +
                           (x(var, 1) - x(i, 1)) * (x(var, 1) - x(i, 1)) +
                           (x(var, 2) - x(i, 2)) * (x(var, 2) - x(i, 2)));
    }
    Col<uword> indexcol, rst_vec;
    rst_vec.set_size(1, 1);
    rst_vec = {i + 1};
    if (with_diag) {
      indexcol = find(distance < 1.5 * l0 && distance > 0) + 1;
    } else {
      indexcol = find(distance < 1.1 * l0 && distance > 0) + 1;
    }
    indexcol.resize(8, 1);
    sup.row(i) = join_horiz(rst_vec, indexcol.t());
  }

  dmg = zeros(N, 3);
}

void NodesObj::copynodes(NodesObj P) {
  x = P.x;
  dmg = P.dmg;
  Ti = P.Ti;
  Tn = P.Tn;
  Ai = P.Ai;
  attached = P.attached;
  init_att = P.init_att;
  sup = P.sup;
  N = P.N;
  l0 = P.l0;
}

double NodesObj::LinT(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i) {
  uword j = (uword)(i - i % 3) / 3;
  if (u(3 * j + 2) * u(3 * j + 2) + u(3 * j + 1) * u(3 * j + 1) + u(3 * j + 0) * u(3 * j + 0) <
      umaxxy * umaxxy) {
    if (i % 3 == 2)
      return kiI * Ai(j) * u(i);
    else
      return kiII * Ai(j) * u(i);
  } else
    return 0; // if detached, no interface contribution
}

double NodesObj::MLinT(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i) {
  double qq = 0;
  if (u(3 * i + 2) * u(3 * i + 2) + u(3 * i + 1) * u(3 * i + 1) + u(3 * i + 0) * u(3 * i + 0) <
      umaxxy * umaxxy) {
    qq += kiII * Ai(i) * u(3 * i + 0) * kiII * Ai(i) * u(3 * i + 0);
    qq += kiII * Ai(i) * u(3 * i + 1) * kiII * Ai(i) * u(3 * i + 1);
    qq += kiI * Ai(i) * u(3 * i + 2) * kiI * Ai(i) * u(3 * i + 2);
    return sqrt(qq);
  } else
    return 0; // if detached, no interface contribution
}

double NodesObj::dLinTd(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i, uword ia) {
  // double exponential = exp(-u(3*i+2)/umaxz -(u(3*i+1)*u(3*i+1) + u(3*i+0)*u(3*i+0))/umaxxy/umaxxy);
  if (u(3 * i + 2) * u(3 * i + 2) + u(3 * i + 1) * u(3 * i + 1) + u(3 * i + 0) * u(3 * i + 0) <
      umaxxy * umaxxy) {
    if (ia == 2)
      return kiI * Ai(i);
    else
      return kiII * Ai(i);
  } else
    return 0; // if detached, no interface contribution
}

double NodesObj::dLinTm(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i, uword ia,
                        uword ib) {
  if (ib == 2 && ia == 2)
    return 0;
  else if (ia == ib)
    return 0;
  else
    return 0; // if detached, no interface contribution
}

double NodesObj::ExpT(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i) {
  uword j = (uword)(i - i % 3) / 3;
  // double exponential = exp(-u(3*j+2)/umaxz -(u(3*j+1)*u(3*j+1) + u(3*j+0)*u(3*j+0))/umaxxy/umaxxy);
  double exponential =
      exp(-(u(3 * j + 2) * u(3 * j + 2) + u(3 * j + 1) * u(3 * j + 1) + u(3 * j + 0) * u(3 * j + 0)) /
          umaxxy / umaxxy);
  if (i % 3 == 2)
    return kiI * Ai(j) * exponential * u(i); //+1E-8/0.002*(u(i)-u2(i))/umaxz;
  else
    return kiII * Ai(j) * exponential * u(i); //+1E-8/0.002*(u(i)-u2(i))/umaxxy;
}

double NodesObj::MExpT(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i) {
  double qq = 0;
  // double exponential = exp(-u(3*i+2)/umaxz -(u(3*i+1)*u(3*i+1) + u(3*i+0)*u(3*i+0))/umaxxy/umaxxy);
  double exponential =
      exp(-(u(3 * i + 2) * u(3 * i + 2) + u(3 * i + 1) * u(3 * i + 1) + u(3 * i + 0) * u(3 * i + 0)) /
          umaxxy / umaxxy);
  qq += kiII * Ai(i) * exponential * u(3 * i + 0) * 0.5 * kiII * Ai(i) * exponential * u(3 * i + 0);
  qq += kiII * Ai(i) * exponential * u(3 * i + 1) * 0.5 * kiII * Ai(i) * exponential * u(3 * i + 1);
  qq += kiI * Ai(i) * exponential * u(3 * i + 2) * 0.5 * kiI * Ai(i) * exponential * u(3 * i + 2);
  return sqrt(qq);
  // return 0.5*ki*Ai(i)*uu/umax*exp(1-uu/umax);
}

double NodesObj::dExpTd(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i, uword ia) {
  // double exponential = exp(-u(3*i+2)/umaxz -(u(3*i+1)*u(3*i+1) + u(3*i+0)*u(3*i+0))/umaxxy/umaxxy);
  double exponential =
      exp(-(u(3 * i + 2) * u(3 * i + 2) + u(3 * i + 1) * u(3 * i + 1) + u(3 * i + 0) * u(3 * i + 0)) /
          umaxxy / umaxxy);

  if (ia == 2)
    return kiI * Ai(i) * exponential * (1 - 2 * u(3 * i + ia) * u(3 * i + ia) / umaxz / umaxz);
  else
    return kiII * Ai(i) * exponential * (1 - 2 * u(3 * i + ia) * u(3 * i + ia) / umaxxy / umaxxy);
}

double NodesObj::dExpTm(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i, uword ia,
                        uword ib) {
  // double exponential = exp(-u(3*i+2)/umaxz -(u(3*i+1)*u(3*i+1) + u(3*i+0)*u(3*i+0))/umaxxy/umaxxy);
  double exponential =
      exp(-(u(3 * i + 2) * u(3 * i + 2) + u(3 * i + 1) * u(3 * i + 1) + u(3 * i + 0) * u(3 * i + 0)) /
          umaxxy / umaxxy);

  if (ib == 2)
    return kiII * Ai(i) * exponential * (-2 * u(3 * i + ia) * u(3 * i + ib) / umaxz / umaxz);
  else if (ia == 2)
    return kiI * Ai(i) * exponential * (-2 * u(3 * i + ia) * u(3 * i + ib) / umaxz / umaxz);
  else
    return kiII * Ai(i) * exponential * (-2 * u(3 * i + ia) * u(3 * i + ib) / umaxxy / umaxxy);
  // return 0.5*ki*Ai(j)*(1-uu/umax)/umax*exp(1-uu/umax);
  // return 0.5*ki*Ai(j)*(1-u(i)/umax)/umax*exp(1-u(i)/umax);
}

void NodesObj::moveNodes(const mat &u) {
  for (int j = 0; j < N; j++) {
    x(j, 0) += u(3 * j + 0);
    x(j, 1) += u(3 * j + 1);
    x(j, 2) += u(3 * j + 2);

    detach_geom_config(u);
  }
}

void NodesObj::detach_geom_config(const mat &u) {
  for (int j = 0; j < N; j++) {
    if (sqrt(u(3 * j + 0) * u(3 * j + 0) + u(3 * j + 1) * u(3 * j + 1) + u(3 * j + 2) * u(3 * j + 2)) >
        l0 * 0.2) {
      set_init_att(j, 0);
      set_attach(j, 0);
    }
  }
}

void NodesObj::attachNodes(umat VC) {
  init_att.zeros();
  attached.zeros();
  for (uword iq = 0; iq < size(VC, 0); ++iq) {
    set_init_att(VC(iq), 1);
    set_attach(VC(iq), 1);
  }
  std::cout << "Nodes attached according to VC\n";
}

void NodesObj::detachNodes(umat VC) {
  for (uword iq = 0; iq < size(VC, 0); ++iq) {
    set_init_att(VC(iq), 0);
    set_attach(VC(iq), 0);
  }
  std::cout << "Nodes detached according to vector\n";
}

void NodesObj::set_attach(uword i, bool value) {
  // std::cout << "We have a detached node here: " << x(i,0)
  // << " " << x(i,1) << std::endl;
  try {
    attached(3 * i + 0, 0) = value;
    attached(3 * i + 1, 0) = value;
    attached(3 * i + 2, 0) = value;
  } catch (...) {
    std::cerr << "failed to attach\n";
  }
}

bool NodesObj::get_attach(uword i) { return attached(3 * i + 0); }

void NodesObj::set_init_att(uword i, bool value) {
  // std::cout << "We have a detached node here: " << x(i,0)
  // << " " << x(i,1) << std::endl;
  try {
    init_att(3 * i + 0, 0) = value;
    init_att(3 * i + 1, 0) = value;
    init_att(3 * i + 2, 0) = value;
  } catch (...) {
    std::cerr << "failed to initially attach\n";
  }
}

bool NodesObj::get_init_att(uword i) { return init_att(3 * i + 0); }

bool NodesObj::update_Ti_att(const Params &P, const vec &u, const vec &u_pre) {
  bool state = true;
  double d = P.umaxI * 2;
  for (uword i = 0; i < N; i++) {
    // Decides if the node is attached of detached
    if (get_attach(i)) {
      if (u(3 * i + 0, 0) * u(3 * i + 0, 0) + u(3 * i + 1, 0) * u(3 * i + 1, 0) +
              u(3 * i + 2, 0) * u(3 * i + 2, 0) >
          d * d) {
        /* if (u(3 * i + 2, 0) * u(3 * i + 2, 0) > d * d) {
         */
        set_attach(i, 0);
        state = false;
      }
    }
    // Calculates the interface tension in the node
    if (!get_init_att(i)) {
      Ti(i, 0) = 1E-9;
    } else
      Ti(i, 0) = MLinT(P.kiI, P.kiII, P.umaxI, P.umaxII, u + u_pre, i);
  }
  return state;
}

void NodesObj::update_Tn_att(const vec &Qi_saved) {
  for (uword i = 0; i < N; i++) {
    // Calculates the membrane tension in the node
    double tempsig = 0.0;
    tempsig += Qi_saved.at(3 * i + 0) * Qi_saved.at(3 * i + 0);
    tempsig += Qi_saved.at(3 * i + 1) * Qi_saved.at(3 * i + 1);
    tempsig += Qi_saved.at(3 * i + 2) * Qi_saved.at(3 * i + 2);
    Tn(i) = sqrt(tempsig);
  }
}