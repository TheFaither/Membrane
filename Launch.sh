#!/bin/bash
dir=()
while IFS= read -d $'\0' -r file ; do
  dir=("${dir[@]}" "$file")
done < <(find . -type d -name "Data*" -print0)

#echo "$dir"
for i in "${dir[@]}"
do
  cp ./Membrane.out ./$i
  echo $i
  ID=${i#*_}
  #ID=$(echo $i | sed -e 's/[^(0-9|)]//g' | sed -e 's/|/,/g')
  $i/Membrane.out -I $ID -d "$i"/ -n 75 >$i/log.txt 2>$i/err.txt &
done

