# Standard plotly imports
import pandas as pd
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import plot as offplot
from plotly.offline import download_plotlyjs, init_notebook_mode, iplot
# Using cufflinks in offline mode
import cufflinks as cf
import numpy as np
import h5py

#init_notebook_mode(connected=True)
cf.go_offline()
#f.set_config_file(world_readable=True, theme='pearl', offline=True)


data = []
clusters = []

mydir = "../Output/"

filename = 'Result2019-12-02--22-53-06_silkcarbonpointhalf.h5'

mf = h5py.File(mydir+filename)
keys = list(mf.keys())
dfx = pd.DataFrame(np.transpose(np.array(mf
                                         ['x'])), columns=['x', 'y', 'z'])

dfu = pd.DataFrame(np.transpose(np.array(mf
                                         ['u'])))

dfu = pd.DataFrame(np.reshape(
    dfu[100].values, (len(dfx['x']), 3, )), columns=['x', 'y', 'z'])

df3 = dfx + dfu
df3.info()


trace = dict(
    x=df3['x'], y=df3['y'], z=df3['z'],
    type="scatter3d",
    mode='markers',
    marker=dict(size=6, color=df3['z'],         colorscale='Jet',
                showscale=True, line=dict(width=0)))
data.append(trace)

layout = dict(
    width=1000,
    height=800,
    autosize=False,
    title='3d plot',
    scene=dict(
        xaxis=dict(
            gridcolor='rgb(255, 255, 255)',
            zerolinecolor='rgb(255, 255, 255)',
            showbackground=True,
            backgroundcolor='rgb(230, 230,230)'
        ),
        yaxis=dict(
            gridcolor='rgb(255, 255, 255)',
            zerolinecolor='rgb(255, 255, 255)',
            showbackground=True,
            backgroundcolor='rgb(230, 230,230)'
        ),
        zaxis=dict(
            gridcolor='rgb(255, 255, 255)',
            zerolinecolor='rgb(255, 255, 255)',
            showbackground=True,
            backgroundcolor='rgb(230, 230,230)'
        ),
        aspectratio=dict(x=1, y=1, z=1),
        aspectmode='manual'
    ),
)

    fig.show()
fig = dict(data=data, layout=layout)
offplot(fig, filename="example.html")

#fig = df.iplot(kind='scatter3d', mode='markers', x='x', y='y',z='z', asFigure=True)
#offplot(fig, filename="example.html")
