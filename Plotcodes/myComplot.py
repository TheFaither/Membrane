# Standard plotly imports
import pandas as pd
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import plot as offplot
from plotly.offline import download_plotlyjs, init_notebook_mode, iplot
# Using cufflinks in offline mode
import cufflinks as cf

init_notebook_mode(connected=True)
cf.go_offline()
#f.set_config_file(world_readable=True, theme='pearl', offline=True)


mydir = "../../Data2018/Data1/Output/"
df1 = pd.read_csv(mydir + 'sig_data.csv',
                 delimiter=' ', header=None, names=['f'])
df2 = pd.read_csv(mydir + 'eps_data.csv',
                  delimiter=' ', header=None, names=['u'])
#df.insert(1, 'u', df2.values, allow_duplicates=True)
#df1.info()
#df2.info()

df = pd.concat([df1, df2], axis=1, sort=False).reset_index()
fig = df.iplot(kind='scatter', mode='markers', x='u', y='f', asFigure=True)
offplot(fig, filename="example.html")
