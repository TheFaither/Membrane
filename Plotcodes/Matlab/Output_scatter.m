clear all;


%cd('../../../Wolff/Output/')
cd('../../Output/')
mydir = dir('*t_nl*');
Mode = 0;

filename = string({mydir.name});
filename = string(filename(size(filename,2)));

u = h5read(filename, "/u");
T = h5read(filename, "/T");
Ti = h5read(filename, "/Ti");
eps = h5read(filename, "/eps");
sig = h5read(filename, "/sig");
x = h5read(filename, "/x");
CMlab = h5read(filename, "/cm");
input = h5read(filename, "/inputs");
%eps = csvread(strcat(dir,'eps_data.csv'));
%sig = csvread(strcat(dir,'sig_data.csv'));

M = figure('Color',[1 1 1]);
hold on;

for number = 89:89%size(u,2)
    cla;
    
    

    

Nx = floor(input(17));
Ny = floor(input(18));

    
    for j = 1:Nx*Ny
        xxx(j) = x(j,1) + u(3*j-2,number);
        yyy(j) = x(j,2) + u(3*j-1,number);
        zzz(j) = x(j,3) + u(3*j,  number);
    end
    
    Tilab = Ti(1:Nx*Ny,number);
    axis on;
    ax = gca;
    ax.FontSize = 40;
    ax.LineWidth = 2;
    ax.FontName = 'Arial';
    %ax.ZTickLabel = ({});
    %ax.YTickLabel = ({});
    
    colormap(M,jet);
    xlabel('x [mm]');
    ylabel('y [mm]');
    
    %title('Membrane Tension');
    %zlim([-inf 0.5]);
    %xlim([0 1]);
    %ylim([0 1]);
    
    
    %view(-10,40);
    view(0,90);
    set(gcf,'units','points','position',[10,10,800,800]);
    [XX,YY] = meshgrid(0:max(x(:,1))/(Nx-1):max(x(:,1)), 0:max(x(:,2))/(Ny-1):max(x(:,2)));
    TilabZ = reshape(Tilab,Nx,Ny);
    TilabZ = TilabZ./max(max(TilabZ));
 
    stressmap = contourf(XX,YY,TilabZ',60,'LineStyle','none');
    %sc3D = scatter3(xxx,yyy,zzz,120,Tilab,'filled');
    %G = delaunayTriangulation(xxx',yyy',zzz');
    %g2 = tetramesh(G);
    %box on;
    
    %     CMap2=colormap(cool(Nx*Ny));
    %     Tilab_sorted=sort(Tilab(:,1));
    %
    %      for j = 1:Nx*Ny
    %          CMap2_ind=find(Tilab_sorted==Tilab(j,1));
    %          xx = [x(j,1) x(j,1) + ulab(3*j-2,1)];
    %          yy = [x(j,2) x(j,2) + ulab(3*j-1,1)];
    %          zz = [x(j,3) x(j,3) + ulab(3*j,1)];
    %          plot3(xx',yy',zz','Color',CMap2(CMap2_ind(1,1),:),'LineWidth',1);
    %      end
    % timestep = (-eps(number)+eps(number+1))/max(eps)*1;
    pause(0.01);
    
end

M.Renderer='Painters';

%hold off;