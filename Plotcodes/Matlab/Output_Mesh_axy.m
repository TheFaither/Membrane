clear all;


%dir = '../../Output/Result2019-04-15--15-21-21_1.hdf';
dir = '../../Output/Result2019-04-20--15-48-15_70.hdf';
eps = h5read(dir, "/eps");
FD = h5read(dir, "/sig");
u = h5read(dir, "/u");
T = h5read(dir, "/T");
Ti = h5read(dir, "/Ti");
x = h5read(dir, "/x");
M = figure('Color',[1 1 1]);
input = h5read(dir, "/inputs");
hold on;
x = x - [0.5 0.5 0];

DataRadius = zeros(200,2);

G=0.05;

Ne = size(FD,1)-1;

for number = 1:Ne
    
    DataRadius(number,2) = FD(number);
    
    if (number < 10)
        index = strcat('-000',num2str(number));
    else if (number < 100)
            index = strcat('-00',num2str(number));
        else if (number < 1000)
                index = strcat('-0',num2str(number));
            else
                index = strcat('-',num2str(number));
            end
        end
    end
    Tilab = Ti(:,number);
    
    K = find(abs(Tilab - max(Tilab)) < max(Tilab)/100);
    
    DataRadius(number,1) = sum(sqrt(x(K,1).*x(K,1) + x(K,2).*x(K,2)))/size(K,1);
    
end

axis on;
P_r = scatter(DataRadius(:,1),DataRadius(:,2),40,'filled');
%eps_r = scatter(DataRadius(1:Ne,1),eps(1:Ne),40,'filled');

%scatter(uD(:),FD(:),40,'filled');


y = 0:0.01:0.5;
%delta = 0:0.02:1.16;
t=0.001;
E=500/(1-0.33^2);
%E=1000;
slope_F = (8/3)^(3/4)*3.14*(t*E)^(1/4)*G^(3/4);
plot(y,y*slope_F,'LineWidth',3);
ylabel('F [N]');
xlabel('r_d [mm]');

%slope_eps = (G*27/E/t/2)^(1/4);
%plot(y,y*slope_eps,'LineWidth',3);
%ylabel('u [mm]');
%xlabel('r_d [mm]');

%slope = (2/3)^(3/2)*2*3.14*(t*E)^(1/2)*G^(1/2);
%plot(y,y*slope,'LineWidth',3);


ax = gca;
ax.FontSize = 40;
ax.LineWidth = 2;
ax.FontName = 'Arial';
xlim([0 max(DataRadius(:,1))*1]);

%scatter(DataRadius(1:Ne,1),eps(1:Ne),40,'filled');

%costante = (2.*eps(1:55).^4)./(27.*(1+DataRadius(1:55,1)).^(4/3).*((1+DataRadius(1:55,1)).^(2/3)-1).^4);
%costante = eps(1:55).^4./DataRadius(1:55,1).^4*2/27;
%Reduced = G/E/t;
%scatter(1:55,costante);
%scatter(DataRadius(1:10,1),y(1,1:10)',40,'filled');
%scatter(DataRadius(1:10,1),y(1:10,1),40,'filled');
%xlim([0 inf]);
