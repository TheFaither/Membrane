clear all;



dir = 'Output_axy2/';
number = 8;

F_data = csvread(strcat(dir,'sig_data.csv'));


M = figure('Color',[1 1 1]);
hold on;



for number = 1:1:11
    cla;
        
    if (number < 10)
        index = strcat('-000',num2str(number));
    else if (number < 100)
            index = strcat('-00',num2str(number));
        else if (number < 1000)
                index = strcat('-0',num2str(number));
            else
                index = strcat('-',num2str(number));
            end
        end
    end
    
    
    ulab = csvread(strcat(dir,'uLab',index,'.csv'));
    x = csvread(strcat(dir,'x.csv'));
    Tlab = csvread(strcat(dir,'Tlab',index,'.csv'));
    Tilab = csvread(strcat(dir,'Tilab',index,'.csv'));
    attached = csvread(strcat(dir,'attached',index,'.csv'));
    CMlab = csvread(strcat(dir,'CMlab.csv'));
    Nx = 51;
    Ny = 51;
    utoview = reshape(ulab',[3 Nx*Ny])';
    x = x - [0.5 0.5 0];
        
    for j = 1:Nx*Ny
        xxx(j) = x(j,1) + ulab(3*j-2,1);
        yyy(j) = x(j,2) + ulab(3*j-1,1);
        zzz(j) = x(j,3) + ulab(3*j,1);
    end
    
    axis on;
    colormap(M,jet);
    xlabel('X (mm)');
    ylabel('Y (mm)');
    title('Membrane Tension');
    zlim([-0.1 5]);
    xlim([-0.6 0.6]);
    ylim([-0.6 0.6]);
    
    
    view(-10,10);
    set(gcf,'units','points','position',[10,10,800,800]);
    
    g2 = scatter3(xxx,yyy,zzz,40,Tilab,'filled');
    %CMap2=colormap(cool(Nx*Ny));
    %Tilab_sorted=sort(Tilab(:,1));
    
    %      for j = 1:Nx*Ny
    %          CMap2_ind=find(Tilab_sorted==Tilab(j,1));
    %          xx = [x(j,1) x(j,1) + ulab(3*j-2,1)];
    %          yy = [x(j,2) x(j,2) + ulab(3*j-1,1)];
    %          zz = [x(j,3) x(j,3) + ulab(3*j,1)];
    %          plot3(xx',yy',zz','Color',CMap2(CMap2_ind(1,1),:),'LineWidth',1);
    %      end
    pause(1);
end

hold off;