clear all;



dir = '../../Output/Result_axy_high_deform_001.hdf';

u = h5read(dir, "/u");
T = h5read(dir, "/T");
Ti = h5read(dir, "/Ti");
eps = h5read(dir, "/eps");
sig = h5read(dir, "/sig");
x = h5read(dir, "/x");
CMlab = h5read(dir, "/cm");
input = h5read(dir, "/inputs");
%eps = csvread(strcat(dir,'eps_data.csv'));
%sig = csvread(strcat(dir,'sig_data.csv'));

M = figure('Color',[1 1 1]);
hold on;

del = 0.3;
rd = del/(0.01*27/2/(500/(1-0.33^2))/0.001)^0.25;
y = 0.5:0.001:0.5+rd;
mzero = zeros(1,size(y,2))+0.5;
zeta = -del*((y-0.5).^(2/3)-rd^(2/3))/rd^(2/3);


for number = 1:1:size(u,1)
    cla;
    
    
    Nx = floor(input(13));
    Ny = floor(input(14));
    
    for j = Nx*(Ny-1)/2:Nx*(Ny-1)/2+Nx
    %for j = 1:Nx*Ny
        xxx(j) = x(j,1) + u(3*j-2,number);
        yyy(j) = x(j,2) + u(3*j-1,number);
        zzz(j) = x(j,3) + u(3*j,  number);
    end
    
    
    
    Tilab = Ti(1:Nx*Ny,number);
    axis on;
    ax = gca;
    ax.FontSize = 40;
    ax.LineWidth = 2;
    ax.FontName = 'Arial';
    %ax.ZTickLabel = ({});
    %ax.YTickLabel = ({});
    
    colormap(jet);
    xlabel('r (mm)');
    zlabel('z (mm)');
    %title('Membrane Tension');
    zlim([-inf inf]);
    xlim([max(xxx)*0.4 inf]);
    ylim([-inf inf]);
    
    
    view(0,0);
    set(gcf,'units','points','position',[10,10,800,800]);
    [XX,YY] = meshgrid(0:max(x(:,1))/(Nx-1):max(x(:,1)), 0:max(x(:,2))/(Ny-1):max(x(:,2)));
    TilabZ = reshape(Tilab,Nx,Ny);
    
    %g2 = contourf(XX,YY,TilabZ',60,'LineStyle','none');
    %g2 = scatter3(xxx,yyy,zzz,120,Tilab,'filled');
    g2 = scatter3(xxx,yyy,zzz,120,'filled');

    plot3(y,mzero,zeta,'LineWidth',3);
    

    %pause(0.01);
    if (zzz((Nx*Ny-1)/2+1) > del)
        break;
    end
end


M.Renderer='Painters';

hold off;
