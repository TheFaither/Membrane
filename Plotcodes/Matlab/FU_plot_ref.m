MI2 = figure('Color', [1 1 1]);
hold on;

dir = 'Output_90';
c = 0;
%
% for i = 10:10:50
%     c = c + 1;
% F_data = csvread(strcat(dir,'_',num2str(i),'/sig_data.csv'));
% u_data = csvread(strcat(dir,'_',num2str(i),'/eps_data.csv'));
% d702 = scatter(u_data(:,1),F_data(:,1));
%     legendInfo{c} = [num2str(i)]; % or whatever is appropriate
%
% end
%
% ylabel('F [N]');
% xlabel('u [mm]');
% legend(legendInfo)

F_data_ref = csvread(strcat(dir, 'ref/sig_data.csv'));
u_data_ref = csvread(strcat(dir, 'ref/eps_data.csv'));
F_data = csvread(strcat(dir, '/sig_data.csv'));
u_data = csvread(strcat(dir, '/eps_data.csv'));

u_data = [u_data' u_data_ref']';
F_data = [F_data' F_data_ref']';
data = [u_data F_data];
[~, idx] = unique(data(:, 1));
data = data(idx, :);

xq1 = 0:0.01:2.5;
p = pchip(data(:, 1), data(:, 2), xq1);
%d701 = plot(xq1,p,'LineWidth',3);
d701 = plot(data(:, 1), data(:, 2), 'LineWidth', 3);
d702 = scatter(u_data(:, 1), F_data(:, 1), 40, 'blue', 'filled');
%figure
%d703 = scatter(Angles(:,1),(Angles(:,2)));
%d704 = plot(x,100*0.001*0.001*(cos(x)-1+sqrt((1-cos(x)).^2 + (2*1e2/100/0.001)) ),'Linewidth',3);
ylabel('F (N)');
xlabel('u (mm)');
ax = gca;
ax.FontSize = 26;
ax.FontName = 'Cambria';
set(gcf, 'units', 'points', 'position', [10, 10, 800, 800]);
%
% %ax.FontAngle = 'italic';
legend('  \theta = 90°', 'Location', 'Northwest');
