cd('Output75');

clear all;


for number =  1 : 1 : 2000
    
    
    dir = 'Output_50/';
    
    if (number < 10)
        index = strcat('-000',num2str(number));
    else if (number < 100)
            index = strcat('-00',num2str(number));
        else if (number < 1000)
                index = strcat('-0',num2str(number));
            else
                index = strcat('-',num2str(number));
            end
        end
    end
    ulab = csvread(strcat(dir,'uLab',index,'.csv'));
    x = csvread(strcat(dir,'x.csv'));
    Tlab = csvread(strcat(dir,'Tlab',index,'.csv'));
    Tilab = csvread(strcat(dir,'Tilab',index,'.csv'));
    attached = csvread(strcat(dir,'attached',index,'.csv'));
    CMlab = csvread(strcat(dir,'CMlab.csv'));
    Nx = 51;
    Ny = 51;
    
    MI = figure('Color',[1 1 1]);
    set(MI, 'Visible', 'off');
    hold on;
    axis equal;
    axis on;
    
    colormap(MI,jet);
    xlabel('X (mm)');
    ylabel('Y (mm)');
    title('Membrane Tension');
    
    xxx = zeros(Nx*Ny,1);
    yyy = zeros(Nx*Ny,1);
    zzz = zeros(Nx*Ny,1);
    
    for j = 1:Nx*Ny
        xxx(j) = x(j,1) + ulab(3*j-2,1);
        yyy(j) = x(j,2) + ulab(3*j-1,1);
        zzz(j) = x(j,3) + ulab(3*j,1);
    end
    xx = [];
    yy = [];
    zz = [];
    black = [0 0 0];
    CMap=colormap(jet(size(Tlab,1)));
    Tlab_sorted=sort(Tlab(:,1));
    for j = 1:size(CMlab,1)
        CMap_ind=find(Tlab_sorted==Tlab(j,1));
        xx = [xxx(CMlab(j,1)+1,1) xxx(CMlab(j,2)+1,1)];
        yy = [yyy(CMlab(j,1)+1,1) yyy(CMlab(j,2)+1,1)];
        zz = [zzz(CMlab(j,1)+1,1) zzz(CMlab(j,2)+1,1)];
        plot3(xx',yy',zz','Color',CMap(CMap_ind(1,1),:),'LineWidth',1);
    end
    zlim([-0.1 0.7]);
    xlim([0 1]);
    ylim([0 1]);
    c1 = colorbar();
    caxis([min(Tlab) max(Tlab)]);
    xlabel(c1,'T');
    view(-12,10);
    saveas(MI,strcat('90_1-',num2str(number),'.png'));
    %view(-12);
    %saveas(MI,strcat('alto-',num2str(number),'.png'));

    
%     M3 = figure('Color',[1 1 1]);
%     hold on;
%     axis equal;
%     axis on;
%     colormap(M3,jet);
%     xlabel('X (mm)');
%     ylabel('Y (mm)');
%     title('Membrane Tension');
%     
%     xx = [];
%     yy = [];
%     zz = [];
%     
%     CMap2=colormap(cool(Nx*Ny));
%     Tilab_sorted=sort(Tilab(:,1));
%     
%     for j = 1:Nx*Ny
%         CMap2_ind=find(Tilab_sorted==Tilab(j,1));
%         xx = [x(j,1) x(j,1) + ulab(3*j-2,1)];
%         yy = [x(j,2) x(j,2) + ulab(3*j-1,1)];
%         zz = [x(j,3) x(j,3) + ulab(3*j,1)];
%         plot3(xx',yy',zz','Color',CMap2(CMap2_ind(1,1),:),'LineWidth',1);
%     end
%     
%     
%     [YY,XX] = meshgrid(0:max(x(:,1))/(Nx-1):max(x(:,1)), 0:max(x(:,2))/(Ny-1):max(x(:,2)));
%     TilabZ = reshape(Tilab,sqrt(Nx*Ny),sqrt(Nx*Ny));
%     
%     figure('Color',[1 1 1]);
%     contourf(YY,XX,TilabZ,60,'LineStyle','none');
%     xlabel('X (mm)');
%     ylabel('Y (mm)');
%     title('Interface Tension');
%     colormap(jet);
%     c = colorbar;
%     caxis([0 0.0353594]);
%     xlabel(c,'Ti');
%     axis equal;
    
end