clear variables;

MI2 = figure('Color',[1 1 1]);
hold on;
xlabel("u [mm]");
ylabel("F [mN]");

ax = gca;
ax.FontSize = 26;
set(gcf,'units','points','position',[10,10,1200,800]);

%cd('../../../Wolff/Data/')
cd('../../Output/')
mydir = dir('*break*');
c = 0;

for mdir = {mydir.name}
    dirname = string(mdir);
    
    
    
    c = c + 1;
    
    try
        
        ud = h5read(dirname, "/eps");
        inputs = h5read(dirname, "/inputs");
        F_sig = h5read(dirname, "/sig")*1000;
        if (size(F_sig,2) > 2)
            Ft = F_sig(:,1);
            Fn = F_sig(:,2);
            Fd = F_sig(:,3);
        else
            Fd = F_sig;
        end
    catch
        warning(strcat('Impossible to create plot for:  ',extractAfter(dirname,"_")));
    end
    
    %input(c,:) = h5read(dirname, "/inputs");
    
    md = [ud Fd];
    
    for count = 1:size(Fd,1)-1
        if (sqrt( 1*(Fd(count) - Fd(count+1))^2 + 1000*(ud(count) - ud(count+1))^2 ) > 10000)
            md(count,:) = [0 0];
            md(count+1,:) = [0 0];
        end
    end
    try
    Fd = md(1:400,2);
    ud = md(1:400,1);
    catch
    Fd = md(:,2);
    ud = md(:,1);    
    end
    %
    %         ud = ud(Fd<4);
    %         Fd = Fd(Fd<4);
    %ud = ud(ud<0.97);
    %Fd = Fd(ud<0.97);
    ud = ud(Fd~=0);
    Fd = Fd(Fd~=0);
    
    if(isempty(ud) == false)
    
    x = 0:0.01:pi/2;
    lim = size(ud,1)*1;
    %lim = 185;
    range = 1:lim;
    
    
    if range ~= 0
        d701 = plot(ud(range,1),Fd(range,1),'-','LineWidth',2);
        %d702 = scatter(u_data(range,1),F_data(range,1));
    else
        d701 = plot(ud(:,1),Fd(:,1),'-');
        %d702 = scatter(u_data(:,1),F_data(:,1));
    end
    %text(u_data(:,1),F_data(:,1),num2str(range(:)));
    %legendInfo{c} = [strcat(extractAfter(dirname,"_"),num2str(inputs(5)))];
    legendInfo{c} = [extractAfter(dirname,"_")];
    else
        c = c-1;
    end
    
end
legend(legendInfo);
set(legend,'Interpreter', 'none');
box on;
set(gcf,'units','points','position',[10,10,600,600]);
ax.FontSize = 40;
ax.LineWidth = 2;

%yline(1000*inputs(3)*inputs(2)/6/(1-cos(deg2rad(inputs(10)))));
%xlim([0 0.8]);
%ylim([0 200]);
ax.FontName = 'Arial';
hold off

%h = findobj(gca,'Type','line');
%X=get(h,'Xdata');
%Y=get(h,'Ydata');
