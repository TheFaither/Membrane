clear all;



dir = 'Output/';
number = 6;

if (number < 10)
    index = strcat('-000',num2str(number));
else if (number < 100)
        index = strcat('-00',num2str(number));
    else if (number < 1000)
            index = strcat('-0',num2str(number));
        else
            index = strcat('-',num2str(number));
        end
    end
end
ulab = csvread(strcat(dir,'uLab',index,'.csv'));
x = csvread(strcat(dir,'x.csv'));
Tlab = csvread(strcat(dir,'Tlab',index,'.csv'));
Tilab = csvread(strcat(dir,'Tilab',index,'.csv'));
attached = csvread(strcat(dir,'attached',index,'.csv'));
CMlab = csvread(strcat(dir,'CMlab.csv'));
Nx = 61;
Ny = 61;
utoview = reshape(ulab',[3 Nx*Ny])';
Tlab = Tlab./max(Tlab).*100;
Tilab = Tilab./max(Tilab).*100;


MI = figure('Color',[1 1 1]);
hold on;
axis equal;
axis on;
colormap(MI,cool);
xlabel('X (mm)');
ylabel('Y (mm)');
zlabel('Z (mm)');
%title('Elastic Energy');
view(-15,20);
ax = gca;
ax.FontSize = 26;
ax.FontSize = 26;
ax.FontName = 'Helvetica';
set(gcf,'units','points','position',[10,10,800,1000]);
c1 = colorbar();
%caxis([0 100]);
xlabel(c1,'Elastic Energy (%)');
xxx = zeros(Nx*Ny,1);
yyy = zeros(Nx*Ny,1);
zzz = zeros(Nx*Ny,1);

for j = 1:Nx*Ny
    xxx(j) = x(j,1) + ulab(3*j-2,1);
    yyy(j) = x(j,2) + ulab(3*j-1,1);
    zzz(j) = x(j,3) + ulab(3*j,1);
end
%     for j = 1:Nx*Ny
%         xxx(j) = x(j,1);
%         yyy(j) = x(j,2);
%         zzz(j) = x(j,3);
%     end

xx = [];
yy = [];
zz = [];
CMap=colormap(cool(size(zzz,1)));
zzz_sorted=sort(zzz(:,1));
for j = 1:size(CMlab,1)
    CMap_ind=find(zzz_sorted==zzz(CMlab(j,1)+1,1,1));
    xx = [xxx(CMlab(j,1)+1,1) xxx(CMlab(j,2)+1,1)];
    yy = [yyy(CMlab(j,1)+1,1) yyy(CMlab(j,2)+1,1)];
    zz = [zzz(CMlab(j,1)+1,1) zzz(CMlab(j,2)+1,1)];
    plot3(xx',yy',zz','Color',CMap(CMap_ind(1,1),:),'LineWidth',1);
end
hold off

M6 = figure('Color', [1 1 1]);
g2 = scatter3(xxx,yyy,zzz,60,zzz,'filled');
axis on;
axis equal;
    zlim([-0.1 inf]);
    xlim([-0.1 1.1]);
    ylim([-0.1 1.1]);






M3 = figure('Color',[1 1 1]);
hold on;
axis equal;
axis on;
xlabel('X (mm)');
ylabel('Y (mm)');
title('Interface Stress');

xx = [];
yy = [];
zz = [];

CMap2=colormap(cool(Nx*Ny));
Tilab_sorted=sort(Tilab(:,1));

for j = 1:Nx*Ny
    CMap2_ind=find(Tilab_sorted==Tilab(j,1));
    xx = [x(j,1) x(j,1) + ulab(3*j-2,1)];
    yy = [x(j,2) x(j,2) + ulab(3*j-1,1)];
    zz = [x(j,3) x(j,3) + ulab(3*j,1)];
    plot3(xx',yy',zz','Color',CMap2(CMap2_ind(1,1),:),'LineWidth',1);
end


[YY,XX] = meshgrid(0:max(x(:,1))/(Nx-1):max(x(:,1)), 0:max(x(:,2))/(Ny-1):max(x(:,2)));
TilabZ = reshape(Tilab,Nx,Ny);








figure('Color',[1 1 1]);
contourf(YY,XX,TilabZ',60,'LineStyle','none');
xlabel('X (%)');
ylabel('Y (%)');
title('Interface stress');
colormap(jet);
c = colorbar;
caxis([0 max(Tilab)]);
xlabel(c,'\sigma/\sigma_{max}');
ax = gca;
ax.FontSize = 26;
axis equal;
