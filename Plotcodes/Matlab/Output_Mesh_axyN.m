clear all;


dir = '../../Output/Result2019-04-11--16-12-44_1.hdf';

F_data = csvread(strcat(dir,'sig_data.csv'));
eps = csvread(strcat(dir,'eps_data.csv'));

M = figure('Color',[1 1 1]);
hold on;

DataRadius = zeros(200,2);

Ne = size(F_data,1)-1;

for number = 1:Ne
    
    DataRadius(number,2) = F_data(number);
    
    if (number < 10)
        index = strcat('-000',num2str(number));
    else if (number < 100)
            index = strcat('-00',num2str(number));
        else if (number < 1000)
                index = strcat('-0',num2str(number));
            else
                index = strcat('-',num2str(number));
            end
        end
    end
    
    
    ulab = csvread(strcat(dir,'uLab',index,'.csv'));
    x = csvread(strcat(dir,'x.csv'));
    Tlab = csvread(strcat(dir,'Tlab',index,'.csv'));
    Tilab = csvread(strcat(dir,'Tilab',index,'.csv'));
    x = x - [0.5 0.5 0];
    K = find(abs(Tilab - max(Tilab)) < max(Tilab)/100);
    %K = find(Tilab < max(Tilab)/10 & Tilab > max(Tilab)/100) ;
    
    DataRadius(number,1) = sum(sqrt(x(K,1).*x(K,1) + x(K,2).*x(K,2)))/size(K,1);
    
end

axis on;
%scatter(DataRadius(1:Ne,1),eps(1:Ne),40,'filled');
%scatter(eps(1:55),F_data(1:55),40,'filled');
D1 = DataRadius(:,1);
D2 = DataRadius(:,2);
%D1 = DataRadius(15:35,1);
%D2 = DataRadius(15:35,2);

y = 0:0.01:6;
r0 = 0.1;

delta = 0:0.02:1.16;
delta = delta./r0;
t=0.01;
E=20/(1-0.33^2);
%E=1000;
G=0.01;
G = G/E/t;
D2 = D2./(2*3.14*E*r0*t);
D1 = D1./r0;
Coeff = 2^(5/4)/27^(1/4) *G^(3/4);
plot(y,(1+y)*Coeff,'LineWidth',3);
scatter(D1,D2,40,'filled');


%slope = (2/3)^(3/2)*2*3.14*(t*E)^(1/2)*G^(1/2);
%plot(delta,delta*slope,'LineWidth',3);

%slope3 = 2*(G*27/E/t/2)^(1/4);
%plot(y,y*slope3,'LineWidth',3);

%scatter(DataRadius(1:Ne,1),eps(1:Ne),40,'filled');

%costante = (2.*eps(1:55).^4)./(27.*(1+DataRadius(1:55,1)).^(4/3).*((1+DataRadius(1:55,1)).^(2/3)-1).^4);
%costante = eps(1:55).^4./DataRadius(1:55,1).^4*2/27;
%Reduced = G/E/t;
%scatter(1:55,costante);
%scatter(DataRadius(1:10,1),y(1,1:10)',40,'filled');
%scatter(DataRadius(1:10,1),y(1:10,1),40,'filled');
%xlim([0 inf]);
%ylim([0 inf]);