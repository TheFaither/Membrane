clear all;


%cd('../../../Wolff/Data/')
cd('../../Output/')
mydir = dir('*Result2019*');
Mode = 0;

filename = {mydir.name};
filename = string(filename(size(filename,2)));

%filename = string(filename(1));


x = h5read(filename, "/x");
CMlab = h5read(filename, "/cm");
input = h5read(filename, "/inputs");
%eps = eps(eps~=0);
Nx = floor(input(17));
Ny = floor(input(18));

cx = input(11);
cl = input(12);

if (Mode == 1)
input(10) = 180-input(10);
end

drag = zeros(size(x,1),1);
for index = 1:size(drag,1)
    if (x(index,1) > (cx-max(x(:,1))/Nx) && x(index,1) < (cx+cl+max(x(:,1))/Nx/2)  && abs(x(index,2) - (max(x(:,2))/2)) < 1E-3)
        drag(index) = 1;
    else
        drag(index) = 0;
    end
end
M = figure('Color',[1 1 1]);
hold on;

    cla;
    
    camproj('perspective')
   
    for j = 1:Nx*Ny
        xxx(j) = x(j,1);
        yyy(j) = x(j,2);
        zzz(j) = x(j,3);
    end
    
    axis on;
    ax = gca;
    ax.FontSize = 40;
    ax.LineWidth = 2;
    ax.FontName = 'Arial';
    %ax.ZTickLabel = ({});
    %ax.YTickLabel = ({});
    
    mycmap = parula(64);
    mycmap = mycmap(2:64,:);
    colormap(M,mycmap);
    xlabel('x [mm]');
    ylabel('y [mm]');
    %xticks(0:0.4:3);
    %yticks(0:0.4:3);
    %zticks([]);
    %ax.CLim = [0 0.3];
    colorbar;
    %title('Membrane Tension');
    zlim([-0.1 inf]);
    xlim([min(min(xxx)-0.2,0) max(max(xxx)+0.2,1.5)]);
    ylim([-inf inf]);
    set(gca,'DataAspectRatioMode','manual')
    set(gca,'DataAspectRatio',[1 1 1])
    set(gca,'PlotBoxAspectRatioMode','manual')
    set(gca,'PlotBoxAspectRatio',[1 1 1])
    
    
    
    view(20,30);
    %view(0,90);
    %view(0,0);
    set(gcf,'units','points','position',[10,10,750,750]);
    [XX,YY] = meshgrid(0:max(x(:,1))/(Nx-1):max(x(:,1)), 0:max(x(:,2))/(Ny-1):max(x(:,2)));

    %     cdx = linspace(input(11),input(11)+input(12),50);
    %     cdy = ones(size(cdx))*input(2)/2;
    %     plot3(cdx,cdy,cdx*sin(input(10)/180*pi),'Linewidth',3);
    
    %stressmap = contourf(XX,YY,TilabZ',60,'LineStyle','none');
    if (Mode == 1)
        zzz = -zzz;
    end
    dpts = [xxx(drag~=0); yyy(drag~=0); zzz(drag~=0)]';
    ppts = [xxx(drag~=1); yyy(drag~=1); zzz(drag~=1)]';
    
    if (abs(input(10) - 90) < 1.5)
        cdz = linspace(dpts(1,3),1+dpts(1,3),50);
        cdx = ones(size(cdz,2),1)*dpts(1,1);
        cdy = ones(size(cdz,2),1)*(max(x(:,2))/2);
        
        forcetread = plot3(cdx,cdy,cdz,'Linewidth',4,'Color','[0.90    0.1250    0.0980]');
        
    else
        if (input(10) > 91.5)
            cdx = linspace(dpts(1,1),2*abs(cos(input(10)/180*pi))+dpts(1,1),50);
            cdy = ones(size(cdx,2),1)*(max(x(:,2))/2);
            forcetread = plot3(cdx,cdy,cdx*tan(-input(10)/180*pi)-cdx(1)*tan(-input(10)/180*pi)+dpts(1,3),'Linewidth',4,'Color','[0.90    0.1250    0.0980]');
        else
            cdx = linspace(-2*(cos(input(10)/180*pi))+dpts(1,1),dpts(1,1),50);
            cdy = ones(size(cdx,2),1)*(max(x(:,2))/2);
            forcetread = plot3(cdx,cdy,cdx*tan(-input(10)/180*pi)-cdx(50)*tan(-input(10)/180*pi)+dpts(1,3),'Linewidth',4,'Color','[0.90    0.1250    0.0980]');
        end
    end
    
    sc3D = scatter3(ppts(:,1),ppts(:,2),ppts(:,3),120,ppts(:,3),'filled');

    pl3Ddrag = plot3(dpts(:,1),dpts(:,2),dpts(:,3),'Linewidth',4,'Color','red');
    sc3Ddrag = scatter3(dpts(:,1),dpts(:,2),dpts(:,3),60,'red','filled');
    
    %G = delaunayTriangulation(xxx',yyy',zzz');
    %g2 = tetramesh(G);
    %box on;

M.Renderer='Painters';

%hold off;