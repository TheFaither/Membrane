clear variables;

MI2 = figure('Color', [1 1 1]);
hold on;
xlabel("u [mm]");
ylabel("F [mN]");

ax = gca;
ax.FontSize = 26;
set(gcf, 'units', 'points', 'position', [10, 10, 1200, 800]);

%cd('../../../Wolff/Data/')
cd('../../Output/')
mydir = dir('*Result*newsig*');
c = 0;

for mdir = {mydir.name}
    dirname = string(mdir);

    c = c + 1;

    try

        ud = h5read(dirname, "/eps");
        inputs = h5read(dirname, "/inputs");
        angles = pi - h5read(dirname, "/angles");
        width = (floor(inputs(18)) - 1) * inputs(19);

        if width == 0
            width = inputs(19) / 2;
        end

        F_sig = h5read(dirname, "/sig") * 1000 / width;

        if (size(F_sig, 2) > 2)
            Ft = F_sig(:, 1);
            Fn = F_sig(:, 2);
            Fd = F_sig(:, 3);
        else
            Fd = F_sig;
        end

    catch
        warning(strcat('Impossible to create plot for:  ', extractAfter(dirname, "_")));
    end

    %input(c,:) = h5read(dirname, "/inputs");

    md = [ud Fd];

    for count = 1:size(Fd, 1) - 1

        if (sqrt(1 * (Fd(count) - Fd(count + 1))^2 + 1000 * (ud(count) - ud(count + 1))^2) > 10000)
            md(count, :) = [0 0];
            md(count + 1, :) = [0 0];
        end

    end

    Fd(:) = md(:, 2);
    ud(:) = md(:, 1);

    ud = ud(Fd ~= 0);
    Fd = Fd(Fd ~= 0);

    ind_ang = 300;

    while angles(ind_ang) > pi * 0.999
        ind_ang = ind_ang - 10;
        angleSP(c) = angles(ind_ang - 20);
    end

    if (isempty(ud) == false)

        x = 0:0.01:pi / 2;
        lim = size(ud, 1);
        extraplot = size(F_sig, 2);
        %lim = 185;
        range = 1:lim;

        if range ~= 0
            d701 = plot(ud(range, 1), Fd(range, 1), '-', 'LineWidth', 2);
            %d702 = scatter(u_data(range,1),F_data(range,1));
        else
            d701 = plot(ud(:, 1), Fd(:, 1), '-');
            %d702 = scatter(u_data(:,1),F_data(:,1));
        end

        if extraplot > 1
            Ft = Ft(Ft ~= 0);
            Fn = Fn(Fn ~= 0);

            plot(ud(:, 1), Fn(:, 1), '--', 'LineWidth', 2);
            plot(ud(:, 1), Ft(:, 1), '--', 'LineWidth', 2);
        end

        %text(u_data(:,1),F_data(:,1),num2str(range(:)));
        legendInfo{c} = [strcat(extractAfter(dirname, "_"))];
    else
        c = c - 1;
    end

end

box on;
set(gcf, 'units', 'points', 'position', [10, 10, 900, 600]);
ax.FontSize = 40;
ax.LineWidth = 2;

%angleSP = deg2rad(inputs(10));
%angleSP = deg2rad(60);
angleSP = angles(100);

syms F_w

pre_eps = 0.0;

Young = inputs(8) * 3/4; %Because without diagonals young is 3/4 of the declared one
Energy = inputs(3);
thickness = inputs(7);
Zeta = Young * thickness / Energy;
F_w_0 = pre_eps * Young * thickness;
Ratio_pre = Zeta * (sqrt(1 + 2 / Zeta) - 1);
Rivlin = F_w * (1 - cos(angleSP)) - Energy == 0;
Kendall = F_w^2 / (2 * Young * thickness) + F_w * (1 - cos(angleSP)) - Energy == 0;
Prestrained = (F_w - F_w_0)^2 / (2 * Young * thickness) + F_w * (1 - cos(angleSP)) - Energy == 0;
%Prestrained = (F_w - F_w_0)^2/(2*Young*thickness)/(1+F_w_0/Young/thickness) + F_w*(1-cos(angleSP)) - Energy == 0;
solK = vpasolve(Kendall, F_w);
solR = vpasolve(Rivlin, F_w);
solP = vpasolve(Prestrained, F_w);
yline(1000 * double(solK(2)), '--', strcat('Kendall \theta = ', num2str(rad2deg(angleSP))));
yline(1000 * double(solR(1)), '-', strcat('Rivlin \theta = ', num2str(rad2deg(angleSP))));
yline(1000 * double(solP(2)), '-', strcat('Prestrained \epsilon = ', num2str(pre_eps)));

legend(legendInfo);
set(legend, 'Interpreter', 'none');

%xlim([0 0.8]);
%ylim([0 200]);
ax.FontName = 'Arial';
hold off

%h = findobj(gca,'Type','line');
%X=get(h,'Xdata');
%Y=get(h,'Ydata');
