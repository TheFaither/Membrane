clear variables;


%cd('../../../Wolff/Data/')
cd('../../Output/')
mydir = dir('*Res*');
Mode = 0;
WhichPlot = 1;

filename = {mydir.name};
filename = string(filename(size(filename,2)));

%filename = string(filename(1));


u = h5read(filename, "/u");
T = h5read(filename, "/T");
Ti = h5read(filename, "/Ti");
eps = h5read(filename, "/eps");

sig = h5read(filename, "/sig");
x = h5read(filename, "/x");
stiffer = h5read(filename, "/stiffer");
CMlab = h5read(filename, "/cm");
input = h5read(filename, "/inputs");
stiffer = h5read(filename, "/stiffer");
%eps = eps(eps~=0);
Nx = floor(input(17));
Ny = floor(input(18));

cx = input(11);
cl = input(12);

if (Mode == 1)
    input(10) = 180-input(10);
end

drag = stiffer;
%drag = zeros(size(x,1));
M = figure('Color',[1 1 1]);
set(gcf,'units','points','position',[10,250,750,450]);
hold on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tn = zeros(size(x,1),size(eps,1));
% for number = 350:400%size(eps,1)
%     for j = 1:size(CMlab,1)
%         k = CMlab(j,1);
%         l = CMlab(j,2);
%         
%         
%         deltal = sqrt( ...
%             (u(3 * (k+1)-2,number) - u(3 * (l+1)-2,number))^2 + ...
%             (u(3 * (k+1) - 1,number) - u(3 * (l+1)-1,number))^2 + ...
%             (u(3 * (k+1),number) - u(3 * (l+1),number))^2 );
%         
%         Tn(k+1,number) = Tn(k+1,number) + deltal;
%         Tn(l+1,number) = Tn(l+1,number) + deltal;
%     end
% end
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for number = 1:size(eps,1)
    cla;
    
    
    for j = 1:Nx*Ny
        xxx(j) = x(j,1) + u(3*j-2,number);
        yyy(j) = x(j,2) + u(3*j-1,number);
        zzz(j) = x(j,3) + u(3*j,  number);
    end
    
    Tilab = Ti(1:Nx*Ny,number);
    %Tnlab = Tn(1:Nx*Ny,number);
    axis on;
    ax = gca;
    ax.FontSize = 20;
    ax.LineWidth = 2;
    ax.FontName = 'Arial';
    %ax.ZTickLabel = ({});
    %ax.YTickLabel = ({});
    
    mycmap = parula(64);
    mycmap = mycmap(2:64,:);
    colormap(gcf,mycmap);
    xlabel('x [mm]');
    ylabel('y [mm]');
    %xticks(0:0.4:3);
    %yticks(0:0.4:3);
    %zticks([]);
    %ax.CLim = [0 0.35];
    colorbar;
    %title('z [mm]');
    %zlim([-0.1 0.5]);
    %xlim([min(min(xxx)-0.1,0) max(max(xxx)+0.1,0.3)]);
    ylim([-inf inf]);
    xlim([-inf inf]);
    set(gca,'DataAspectRatioMode','manual')
    set(gca,'DataAspectRatio',[1 1 1])
    set(gca,'PlotBoxAspectRatioMode','manual')
    set(gca,'PlotBoxAspectRatio',[1 1 1])
    
    
    
    
    [XX,YY] = meshgrid(0:max(x(:,1))/(Nx-1):max(x(:,1)), 0:max(x(:,2))/(Ny-1):max(x(:,2)));
    TilabZ = reshape(Tilab,Nx,Ny);
    TilabZ = TilabZ./max(max(TilabZ));
    %TnlabZ = reshape(Tnlab,Nx,Ny);
    %TnlabZ = TnlabZ./max(max(TnlabZ));
    
    
    if (WhichPlot == 0)
        stressmap = contourf(XX,YY,TilabZ',60,'LineStyle','none');
    else
        camproj('perspective')
        xlim([min(min(xxx)-0.1,0) max(max(xxx)+0.1,0.3)]);
        ylim([-inf inf]);
        %view(-20,30);
        view(20,30);
        %view(0,90);
        %view(0,0);
        if (Mode == 1)
            zzz = -zzz;
        end
        dpts = [xxx(drag~=0); yyy(drag~=0); zzz(drag~=0)]';
        ppts = [xxx(drag~=1); yyy(drag~=1); zzz(drag~=1)]';
        
        %         if (abs(input(10) - 90) < 1.5)
%         cdz = linspace(dpts(1,3),1+dpts(1,3),50);
%         cdx = ones(size(cdz,2),1)*dpts(1,1);
%         cdy = ones(size(cdz,2),1)*(max(x(:,2))/2);
        
%         forcetread = plot3(cdx,cdy,cdz,'Linewidth',4,'Color','[0.90    0.1250    0.0980]');
        %
        %         else
                    if (input(10) > 91.5)
                        cdx = linspace(dpts(1,1),2*abs(cos(input(10)/180*pi))+dpts(1,1),50);
                        cdy = ones(size(cdx,2),1)*(max(x(:,2))/2);
                        %forcetread = plot3(cdx,cdy,cdx*tan(-input(10)/180*pi)-cdx(1)*tan(-input(10)/180*pi)+dpts(1,3),'Linewidth',4,'Color','[0.90    0.1250    0.0980]');
                    else
                        cdx = linspace(-2*(cos(input(10)/180*pi))+dpts(1,1),dpts(1,1),50);
                        cdy = ones(size(cdx,2),1)*(max(x(:,2))/2);
                        %forcetread = plot3(cdx,cdy,cdx*tan(-input(10)/180*pi)-cdx(50)*tan(-input(10)/180*pi)+dpts(1,3),'Linewidth',4,'Color','[0.90    0.1250    0.0980]');
                    end
                    %         end
        
        scd3D = scatter3(dpts(:,1),dpts(:,2),dpts(:,3),90,'red','filled');
        %scd3D = scatter3(dpts(:,1),dpts(:,2),dpts(:,3),90,Tnlab(drag~=0),'filled');
        %sc3D = scatter3(ppts(:,1),ppts(:,2),ppts(:,3),90,Tilab(drag~=1),'filled');
        %sc3D = scatter3(ppts(:,1),ppts(:,2),ppts(:,3),90,Tnlab(drag~=1),'filled');
        sc3D = scatter3(ppts(:,1),ppts(:,2),ppts(:,3),80,ppts(:,3),'filled');
        
        %pl3Ddrag = plot3(dpts(:,1),dpts(:,2),dpts(:,3),'Linewidth',4,'Color','red');
        sc3Ddrag = scatter3(dpts(:,1),dpts(:,2),dpts(:,3),80,'red','filled');
        
    end

    drawnow;
    
end

M.Renderer='Painters';

%hold off;