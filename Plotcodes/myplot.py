# Nice util
import pandas as pd
import numpy as np
import h5py
import plotly.express as px

mydir = "../Output/"
filename = 'Result2019-12-02--22-53-06_silkcarbonpointhalf.h5'
mf = h5py.File(mydir+filename)

dP = pd.DataFrame(np.transpose(np.array(mf
                                        ['sig'])), columns=['sig'])
dP['eps'] = pd.DataFrame(np.transpose(np.array(mf
                                         ['eps'])), columns=['eps'])

fig = px.scatter(dP, x = 'eps', y = 'sig')

fig.show()
