# Standard plotly imports
import pandas as pd
import chart_studio as py
# Using cufflinks in offline mode
import cufflinks as cf
import numpy as np
import h5py
import plotly.express as px
from plotly.offline import plot as offplot

cf.go_offline()

mydir = "../Output/"
filename = 'Result2019-12-02--22-53-06_silkcarbonpointhalf.h5'
mf = h5py.File(mydir+filename)

dfx = pd.DataFrame(np.transpose(np.array(mf
                                         ['x'])), columns=['x', 'y', 'z'])

dfu = pd.DataFrame(np.transpose(np.array(mf
                                         ['u'])))
dfus = pd.DataFrame(np.reshape(
    dfu[0].values, (len(dfx['x']), 3)), columns=['x', 'y', 'z'])
dfus = dfus + dfx
dfus['time'] = 0


#  Create dataframe for every step, define a time that will form the index for the animation.
#! This is not the optimal solution.
#? Maybe Pandas have a way to do it quickly?

for k in range(400,430):
	dfuk = pd.DataFrame(np.reshape(
		dfu[k].values, (len(dfx['x']), 3)), columns=['x', 'y', 'z'])
	dfuk = dfuk + dfx
	dfuk['time']=k
	dfus = pd.concat([dfus,dfuk], ignore_index=True, keys=['1','2'])

dfus.info()

#! Scatter_3d actually does not support smooth animations. Fuck.
fig = px.scatter_3d(dfus, x='x', y='y', z='z',
                    color='z', animation_frame='time', hover_name="z", range_z=[0, 3])
fig.show()
