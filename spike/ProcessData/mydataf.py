import pandas as pd
import os
import glob
import numpy as np

mydir = "../Data2018/Data1/Output/"
InitialPositions = pd.read_csv(os.path.join(mydir, 'x.csv'),
    delimiter=',', header=None, names=['x', 'y', 'z'])

all_files = glob.glob(os.path.join(mydir, "uLab-000[0-3].csv"))

N = int((len(pd.read_csv(all_files[1]))/3))+1

list_ = []

for f in all_files:
    df = pd.DataFrame(np.reshape(
        (pd.read_csv(f, index_col=None, header=None) ).values, (N, 3)), columns=['x', 'y', 'z']
    )

    list_.append(InitialPositions + df)

df2 = pd.concat(list_, axis=1)


writer = pd.ExcelWriter('output.xlsx')
df2.to_excel(writer, 'Sheet1')
writer.save()
