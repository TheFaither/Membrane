clear all;

cd('../Output/')
mydir = dir('*tocreategrid*50*');
Mode = 0;

filename = string({mydir.name});
filename = string(filename(size(filename,2)));


x = h5read(filename, "/x");
input = h5read(filename, "/inputs");


Nx = floor(input(17));
Ny = floor(input(18));

sidex = input(1);
sidey = input(2);
l0 = input(19);
oldx = x;

yc = sidey/2 - l0;
cx = input(11);
cl = input(12);
r = 0.2;
h = 0.2;

p1p = [0,r,0];
p1n = [0,-r,0];
p2 = [0,0,h];
p3 = [cl,0,0];

normalp = cross(p1p - p2, p1p - p3);
dp = p1p(1)*normalp(1) + p1p(2)*normalp(2) + p1p(3)*normalp(3);

normaln = cross(p1n - p2, p1n - p3);
dn = p1n(1)*normaln(1) + p1n(2)*normaln(2) + p1n(3)*normaln(3);

for i = 1:Nx*Ny
    if (x(i,1) - cx <= 0)
        if ((x(i,1) - cx)^2 + (x(i,2) - yc)^2 < r^2)
            x(i,3) = h/r*(r - sqrt((x(i,1) - cx)^2 + (x(i,2) - yc)^2));
        end
    else
        if (x(i,2) < yc)
            Zp = (dp - (normalp(1)*(x(i,1)-cx) - (normalp(2)*(x(i,2)-yc))))/normalp(3);
            if (Zp > 0)
                x(i,3) = Zp;
            end
        else
            Zn = (dn - (normaln(1)*(x(i,1)-cx) - (normaln(2)*(x(i,2)-yc))))/normaln(3);
            if (Zn > 0)
                x(i,3) = Zn;
            end
        end
    end
end

u = reshape((x - oldx)',[Nx*Ny*3 1]);
cd('../Input/');
dlmwrite(strcat('geom_config.csv'),u,'delimiter',',');