clear all;

cd('../Output/')
mydir = dir('*n80*');
Mode = 0;
filename = {mydir.name};
filename = string(filename(size(filename,2)));

%filename = string(filename(1));

eps = h5read(filename, "/eps");
u = h5read(filename, "/u");

expected_pull = 0.2;
indexes = find((eps-expected_pull)>0);



number = indexes(1);

u_save = u(:,number);

cd('../Input/');
dlmwrite(strcat('geom_config.csv'),u_save,'delimiter',',');