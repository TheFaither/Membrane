
/**
 * @brief index creator
 *
 * @param i original number
 * @return std::string correct index
 */
std::string uniquePathFileNamePostFix(uword i) {
  std::stringstream ss;
  if (i < 10)
    ss << '-' << '0' << '0' << '0' << i;
  else if (i < 100)
    ss << '-' << '0' << '0' << i;
  else if (i < 1000)
    ss << '-' << '0' << i;
  else
    ss << '-' << i;
  return (ss.str());
}

/**
 * @brief command-line value reader
 *
 * @param ind index
 * @param argc argc
 * @param argv argv
 * @param endptr pointer for argv
 * @return long int read value
 */
long int read_values(int ind, int argc, char *argv[], char *endptr) {
  long int input = strtol(argv[ind], &endptr, 10);
  if (!*argv[ind] || *endptr)
    cerr << "Invalid number " << argv[ind] << '\n';
  std::cout << "arg" << ind << ": " << input << std::endl;
  return input;
}

// pow(NC.Nodes_del.x.col(1) - cstart,2)) >= 4.5*mM.l0);
/*uvec VC = find(
        ( (NC.Nodes_del.x.col(0) > 2*(3*mM.l0+10*mM.l0) + 0.5*mM.l0) &&
   (NC.Nodes_del.x.col(0) < 2*(3*mM.l0+10*mM.l0) + 3.5*mM.l0)
        || (NC.Nodes_del.x.col(0) > 3*(3*mM.l0+10*mM.l0) + 0.5*mM.l0) &&
   (NC.Nodes_del.x.col(0) < 3*(3*mM.l0+10*mM.l0) + 3.5*mM.l0)
        || (NC.Nodes_del.x.col(0) > 4*(3*mM.l0+10*mM.l0) + 0.5*mM.l0) &&
   (NC.Nodes_del.x.col(0) < 4*(3*mM.l0+10*mM.l0) + 3.5*mM.l0)
        || (NC.Nodes_del.x.col(0) > 1*(3*mM.l0+10*mM.l0) + 0.5*mM.l0) &&
   (NC.Nodes_del.x.col(0) < 1*(3*mM.l0+10*mM.l0) + 3.5*mM.l0)
        )
        && (
        (NC.Nodes_del.x.col(1) > 2*(3*mM.l0+10*mM.l0)) +
   0.5*mM(NC.Nodes_del.x.col(1) < 2*(3*mM.l0+10*mM.l0) + 3.5*mM.l0)
        || (NC.Nodes_del.x.col(1) > 3*(3*mM.l0+10*mM.l0)) +
   0.5*mM(NC.Nodes_del.x.col(1) < 3*(3*mM.l0+10*mM.l0) + 3.5*mM.l0)
        || (NC.Nodes_del.x.col(1) > 4*(3*mM.l0+10*mM.l0)) +
   0.5*mM(NC.Nodes_del.x.col(1) < 4*(3*mM.l0+10*mM.l0) + 3.5*mM.l0)
        || (NC.Nodes_del.x.col(1) > 1*(3*mM.l0+10*mM.l0)) +
   0.5*mM(NC.Nodes_del.x.col(1) < 1*(3*mM.l0+10*mM.l0) + 3.5*mM.l0)
        )
    );
*/