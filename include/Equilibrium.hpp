//
//  ArcLength.hpp
//  MP
//
//  Created by Daniele on 12/02/2017.
//  Copyright © 2017 DL. All rights reserved.
//

#ifndef ArcLength_hpp
#define ArcLength_hpp

#include "SpringsObj.hpp"

using namespace arma;

const double PENALTY = 1E20;

void DisplacementControl(vec &u, vec &Qe, vec &Qi_saved, NodesCollec &NC, SpringsObj &Springs, mat u_pre,
                         uvec TC, uvec BC, const Params &prms, rowvec &sig, double &eps,
                         double &prestrain, double &theta, double phi, bool &success, double givenalpha);

void ArcLength(vec &u, vec &Qe, vec &Qi_saved, vec &Du0, double &lambda, NodesCollec &NC,
               SpringsObj &Springs, vec u_pre, uvec TC, uvec BC, const Params &prms, rowvec &sig,
               double &eps, double &prestrain, bool &success, bool &lownorm);

sp_mat sys_eq(vec &u, NodesCollec &NC, SpringsObj &Springs, const double &prestrain, vec u_pre,
              const Params &prms);

void build_Qi(vec &u, vec &Qi, NodesCollec &NC, SpringsObj &Springs, const double &prestrain, vec u_pre,
              const Params &prms);

void apply_interface(NodesCollec NC, const vec u, const Params prms, umat &locations, mat &Kc);

inline sp_mat sys_eq(vec &u, vec &Qi, NodesCollec &NC, SpringsObj &Springs, const double &prestrain,
                     vec u_pre) {
  const double n3d = 36;
  int limit = Springs.N;

  // Initialisation of the stiffness matrix using sparse formulation
  umat locations(2 * n3d * limit, 2);
  mat Kc(2 * n3d * limit, 1);
  locations.zeros();
  Kc.zeros();

  mat diagonal = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

  mat klm(3, 3);
  mat klg(3, 3);
  mat n_sqr(3, 3);
  for (int j = 0; j < limit; j++) {
    //    if (Springs.broken(j) < 0.5) {
    uword k = Springs.cm(j, 0); // 1st node of the spring
    uword l = Springs.cm(j, 1); // 2nd node of the spring

    umat sup = {3 * k, 3 * k + 1, 3 * k + 2,
                3 * l, 3 * l + 1, 3 * l + 2}; // Vector of spring coordinates
    sup = sup.t();

    Springs.l.at(j, 0) =
        sqrt(pow((NC.Nodes.x(k, 0) + u(3 * k) - NC.Nodes.x(l, 0) - u(3 * l)), 2) +
             pow((NC.Nodes.x(k, 1) + u(3 * k + 1) - NC.Nodes.x(l, 1) - u(3 * l + 1)), 2) +
             pow((NC.Nodes.x(k, 2) + u(3 * k + 2) - NC.Nodes.x(l, 2) - u(3 * l + 2)), 2));

    Springs.dl.at(j, 0) =
        (Springs.l.at(j, 0) * Springs.l.at(j, 0) - (Springs.l0.at(j, 0) * Springs.l0.at(j, 0))) /
        (Springs.l.at(j, 0) + Springs.l0.at(j, 0));

    // Compute the local material and geometric stiffness matrix
    double n1 =
        (NC.Nodes.x.at(l, 0) + u.at(3 * l) - NC.Nodes.x.at(k, 0) - u.at(3 * k)) / Springs.l.at(j, 0);
    double n2 = (NC.Nodes.x.at(l, 1) + u.at(3 * l + 1) - NC.Nodes.x.at(k, 1) - u.at(3 * k + 1)) /
                Springs.l.at(j, 0);
    double n3 = (NC.Nodes.x.at(l, 2) + u.at(3 * l + 2) - NC.Nodes.x.at(k, 2) - u.at(3 * k + 2)) /
                Springs.l.at(j, 0);

    n_sqr.zeros();
    n_sqr = {{n1 * n1, n1 * n2, n1 * n3}, {n1 * n2, n2 * n2, n2 * n3}, {n1 * n3, n2 * n3, n3 * n3}};
    mat n_n = {-n1, -n2, -n3, n1, n2, n3};

    Springs.T(j, 0) = Springs.getEA(j, 0) / Springs.l0(j, 0) * Springs.dl(j, 0);
    double T_Prestrain = Springs.getEA(j, 0) * prestrain;

    klm.zeros();
    klg.zeros();
    klm = Springs.getEA(j, 0) / Springs.l0(j, 0) * n_sqr;
    klg = (Springs.T(j, 0) + T_Prestrain) / Springs.l(j, 0) * diagonal;

    mat km(klm);
    km = join_horiz(km, -km);
    km = join_vert(km, -km);

    mat kg(klg);
    kg = join_horiz(kg, -kg);
    kg = join_vert(kg, -kg);

    // Assembly in the global stiffness matrix
    locations(span(j * n3d, (j + 1) * n3d - 1), 0) = repmat(sup, 6, 1);
    locations(span(j * n3d, (j + 1) * n3d - 1), 1) =
        sort(locations(span(j * n3d, (j + 1) * n3d - 1), 0));
    Kc.rows(j * n3d, (j + 1) * n3d - 1) = reshape(km, n3d, 1);

    locations(span(n3d * limit + j * n3d, n3d * limit + (j + 1) * n3d - 1), 0) = repmat(sup, 6, 1);
    locations(span(n3d * limit + j * n3d, n3d * limit + (j + 1) * n3d - 1), 1) =
        sort(locations(span(n3d * limit + j * n3d, n3d * limit + (j + 1) * n3d - 1), 0));
    Kc.rows(n3d * limit + j * n3d, n3d * limit + (j + 1) * n3d - 1) = reshape(kg, n3d, 1);

    // Update internal forces
    mat Qi_mat = (Springs.T(j, 0) + T_Prestrain) * n_n;
    for (uword i = 0; i < sup.n_elem; ++i) {
      Qi(sup(i)) += Qi_mat(0, i);
    }

    //    } else
    //      Springs.T(j) = 0;
  } // end springs for
  locations = locations.t();
  return sp_mat(true, locations, Kc, 3 * NC.Nodes.N, 3 * NC.Nodes.N, true, true);
}

inline sp_mat interface_lin(vec &u, vec &Qi, NodesCollec &NC, SpringsObj &Springs, uvec &TC, uvec &BC,
                            const double &prestrain, vec u_pre, const Params &prms) {
  double umaxI = prms.umaxI;
  double umaxII = prms.umaxII;
  double kiI = prms.kiI;
  double kiII = prms.kiII;

  const double n3d = 36;
  int limit = Springs.N;

  // Initialisation of the stiffness matrix using sparse formulation
  umat locations(2 * n3d * limit, 2);
  mat Kc(2 * n3d * limit, 1);
  locations.zeros();
  Kc.zeros();

  // Applying interface contribuition
  umat InterfLoc(9 * NC.Nodes.N, 2);
  InterfLoc.fill(0);
  urowvec couple(2);
  mat InterfKc = zeros(9 * NC.Nodes.N, 1);
  for (uword i = 0; i < NC.Nodes.N; ++i) {
    for (uword ib = 0; ib < 3; ++ib) {
      for (uword ia = 0; ia < 3; ++ia) {

        couple << 3 * i + ia << 3 * i + ib;
        InterfLoc.row(9 * i + ia + 3 * ib) = couple;
        if (ia == ib) {
          if (NC.Nodes.init_att(3 * i, 0) == 0)
            InterfKc(9 * i + ia + 3 * ib, 0) =
                NC.Nodes.dLinTd(kiI * 1E-3, kiII * 1E-3, umaxI, umaxII, u, i, ib);
          else
            InterfKc(9 * i + ia + 3 * ib, 0) = NC.Nodes.dLinTd(kiI, kiII, umaxI, umaxII, u, i, ib);
        } else {
          if (NC.Nodes.init_att(3 * i, 0) == 0)
            InterfKc(9 * i + ia + 3 * ib, 0) =
                NC.Nodes.dLinTm(kiI * 1E-3, kiII * 1E-3, umaxI, umaxII, u, i, ib, ia);
          else
            InterfKc(9 * i + ia + 3 * ib, 0) = NC.Nodes.dLinTm(kiI, kiII, umaxI, umaxII, u, i, ib, ia);
        }
      }
    }
  }

  for (uword var = 0; var < Qi.n_rows; ++var) {
    if (NC.Nodes.init_att(var, 0) == 0)
      Qi(var) += NC.Nodes.LinT(kiI * 1E-3, kiII * 1E-3, umaxI, umaxII, u + u_pre, var);
    else
      Qi(var) += NC.Nodes.LinT(kiI, kiII, umaxI, umaxII, u + u_pre, var);
  }
  locations = join_vert(locations, InterfLoc);
  Kc = join_vert(Kc, InterfKc);

  locations = locations.t();

  return sp_mat(true, locations, Kc, 3 * NC.Nodes.N, 3 * NC.Nodes.N, true, true);
}

inline sp_mat interface_exp(vec &u, vec &Qi, NodesCollec &NC, SpringsObj &Springs, uvec &TC, uvec &BC,
                            const double &prestrain, vec u_pre, const Params &prms) {
  double umaxI = prms.umaxI;
  double umaxII = prms.umaxII;
  double kiI = prms.kiI;
  double kiII = prms.kiII;

  const double n3d = 36;
  int limit = Springs.N;

  // Initialisation of the stiffness matrix using sparse formulation
  umat locations(2 * n3d * limit, 2);
  mat Kc(2 * n3d * limit, 1);
  locations.zeros();
  Kc.zeros();

  // Applying interface contribuition
  umat InterfLoc(9 * NC.Nodes.N, 2);
  InterfLoc.fill(0);
  urowvec couple(2);
  mat InterfKc = zeros(9 * NC.Nodes.N, 1);
  for (uword i = 0; i < NC.Nodes.N; ++i) {
    for (uword ib = 0; ib < 3; ++ib) {
      for (uword ia = 0; ia < 3; ++ia) {

        couple << 3 * i + ia << 3 * i + ib;
        InterfLoc.row(9 * i + ia + 3 * ib) = couple;
        if (ia == ib) {
          if (NC.Nodes.init_att(3 * i, 0) == 0)
            InterfKc(9 * i + ia + 3 * ib, 0) =
                NC.Nodes.dExpTd(kiI * 1E-3, kiII * 1E-3, umaxI, umaxII, u, i, ib);
          else
            InterfKc(9 * i + ia + 3 * ib, 0) = NC.Nodes.dExpTd(kiI, kiII, umaxI, umaxII, u, i, ib);
        } else {
          if (NC.Nodes.init_att(3 * i, 0) == 0)
            InterfKc(9 * i + ia + 3 * ib, 0) =
                NC.Nodes.dExpTm(kiI * 1E-3, kiII * 1E-3, umaxI, umaxII, u, i, ib, ia);
          else
            InterfKc(9 * i + ia + 3 * ib, 0) = NC.Nodes.dExpTm(kiI, kiII, umaxI, umaxII, u, i, ib, ia);
        }
      }
    }
  }

  for (uword var = 0; var < Qi.n_rows; ++var) {
    if (NC.Nodes.init_att(var, 0) == 0)
      Qi(var) += NC.Nodes.ExpT(kiI * 1E-3, kiII * 1E-3, umaxI, umaxII, u + u_pre, var);
    else
      Qi(var) += NC.Nodes.ExpT(kiI, kiII, umaxI, umaxII, u + u_pre, var);
  }
  locations = join_vert(locations, InterfLoc);
  Kc = join_vert(Kc, InterfKc);

  locations = locations.t();

  return sp_mat(true, locations, Kc, 3 * NC.Nodes.N, 3 * NC.Nodes.N, true, true);
}

inline sp_mat K_constraints(NodesCollec &NC, SpringsObj &Springs, uvec &TC, uvec &BC) {

  const double n3d = 36;
  int limit = Springs.N;

  // Initialisation of the stiffness matrix using sparse formulation
  umat locations(2 * n3d * limit, 2);
  mat Kc(2 * n3d * limit, 1);
  locations.zeros();
  Kc.zeros();

  // Applying traction penalties
  umat TInterfLoc(3 * TC.n_elem, 2);
  mat TInterfKc(3 * TC.n_elem, 1);
  for (uword i = 0; i < TC.n_elem; ++i) {
    urowvec couple_x(2);
    urowvec couple_y(2);
    urowvec couple_z(2);
    couple_x.fill(TC(i) * 3 + 0);
    couple_y.fill(TC(i) * 3 + 1);
    couple_z.fill(TC(i) * 3 + 2);
    TInterfLoc.row(3 * i + 0) = couple_x;
    TInterfLoc.row(3 * i + 1) = couple_y;
    TInterfLoc.row(3 * i + 2) = couple_z;
    TInterfKc.at(3 * i + 0) = PENALTY;
    TInterfKc.at(3 * i + 1) = PENALTY;
    TInterfKc.at(3 * i + 2) = PENALTY;
  }

  // Applying boundary penalties
  umat BInterfLoc(3 * BC.n_elem, 2);
  mat BInterfKc(3 * BC.n_elem, 1);
  for (uword i = 0; i < BC.n_elem; ++i) {
    urowvec couple_x(2);
    urowvec couple_y(2);
    urowvec couple_z(2);
    couple_x.fill(BC(i) * 3 + 0);
    couple_y.fill(BC(i) * 3 + 1);
    couple_z.fill(BC(i) * 3 + 2);
    BInterfLoc.row(3 * i + 0) = couple_x;
    BInterfLoc.row(3 * i + 1) = couple_y;
    BInterfLoc.row(3 * i + 2) = couple_z;
    BInterfKc.at(3 * i + 0) = PENALTY;
    BInterfKc.at(3 * i + 1) = PENALTY;
    BInterfKc.at(3 * i + 2) = PENALTY;
  }

  locations = join_vert(locations, TInterfLoc);
  Kc = join_vert(Kc, TInterfKc);
  locations = join_vert(locations, BInterfLoc);
  Kc = join_vert(Kc, BInterfKc);

  locations = locations.t();
  return sp_mat(true, locations, Kc, 3 * NC.Nodes.N, 3 * NC.Nodes.N, true, true);
}

inline void Qi_constraints(vec &Qi, uvec &TC, uvec &BC) {
  // Applying traction boundaries
  for (uword iq = 0; iq < size(TC, 0); ++iq) {
    Qi.at(3 * TC(iq) + 0) = 0.0;
    Qi.at(3 * TC(iq) + 1) = 0.0;
    Qi.at(3 * TC(iq) + 2) = 0.0;
  }

  // Applying external boundaries
  for (uword iq = 0; iq < size(BC, 0); ++iq) {
    Qi.at(3 * BC(iq) + 0) = 0.0;
    Qi.at(3 * BC(iq) + 1) = 0.0;
    Qi.at(3 * BC(iq) + 2) = 0.0;
  }
}

inline mat setnulls(mat &matrix) {
  for (uword i = 0; i < matrix.n_rows; ++i) {
    if (std::abs(matrix(i)) < 1E-14 * PENALTY) {
      matrix(i) /= PENALTY;
    }
  }
  return matrix;
}

#endif /* Arclength */
