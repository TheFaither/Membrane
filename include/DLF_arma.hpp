#ifndef DLF_arma
#define DLF_arma

#include <armadillo>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <random>
#include <signal.h>
#include <stdio.h>
#include <sys/stat.h>

using namespace arma;

template <typename T> int sgn(T val) { return (T(0) < val) - (val < T(0)); }

template <typename T> inline bool approx_equal_cpp(const T &lhs, const T &rhs, double tol = 0.00000001) {
  return arma::approx_equal(lhs, rhs, "absdiff", tol);
}

inline arma::umat unique_rows(const arma::umat &m) {

  arma::uvec ulmt = arma::zeros<arma::uvec>(m.n_rows);
  std::cout << m.n_rows << " to calculate" << std::endl;

  for (arma::uword i = 0; i < m.n_rows; i++) {
    for (arma::uword j = i + 1; j < m.n_rows; j++) {
      if (m.at(i, 0) == m.at(j, 0) && m.at(i, 1) == m.at(j, 1)) {
        ulmt(j) = 1;
        break;
      }
    }
  }

  return m.rows(find(ulmt == 0));
}

inline arma::umat delete_zeros(const arma::umat &m) {

  arma::umat m_c(m);
  arma::uword i = 0;

  for (i = 0; i < m.n_rows; i++) {
    if (m.at(i, 0) == 0 && m.at(i, 1) == 0) {
      break;
    }
  }
  m_c.shed_rows(i, m.n_rows - 1);
  return m_c;
}

inline arma::mat delete_minus(const arma::mat &m) {

  arma::mat m_c(m);
  arma::uword i = 0, c = 0;

  for (i = 0; c < m.n_rows; i++, c++) {
    if (m_c(i, 0) == -1.0 && m_c(i, 1) == -1.0 && m_c(i, 2) == -1.0) {
      try {
        m_c.shed_row(i);
      } catch (...) {
        std::cerr << "failed at " << c << std::endl;
      }
      i--;
    }
  }
  return m_c;
}

inline arma::umat unique_cols(const arma::umat &m) {

  arma::uvec vlmt = arma::zeros<arma::uvec>(m.n_cols);

  for (arma::uword i = 0; i < m.n_cols; i++) {
    for (arma::uword j = i + 1; j < m.n_cols; j++) {
      if (approx_equal_cpp(m.col(i), m.col(j))) {
        vlmt(j) = 1;
        break;
      }
    }
  }

  return m.cols(find(vlmt == 0));
}

inline void pull_points(vec &Qe, uvec &TC, double theta_pull) {
  for (uword iq = 0; iq < size(TC, 0); ++iq) {
    Qe(3 * TC(iq) + 0) = cos(theta_pull);
    Qe(3 * TC(iq) + 1) = 0.0;
    Qe(3 * TC(iq) + 2) = sin(theta_pull);
  }
}

inline void compress(vec &Qe, uvec &TC, mat &x, double length_step) {
  rowvec maxx = max(x);
  for (uword iq = 0; iq < size(TC, 0); ++iq) {
    if (x.at(TC(iq), 0) < 0.5 * length_step) {
      Qe(3 * TC(iq) + 0) = 1;
    } else if (x.at(TC(iq), 0) > maxx(0) - 0.5 * length_step) {
      Qe(3 * TC(iq) + 0) = -1;
    }
    if (x.at(TC(iq), 1) < 0.5 * length_step) {
      Qe(3 * TC(iq) + 1) = 1;
    } else if (x.at(TC(iq), 1) > maxx(1) - 0.5 * length_step) {
      Qe(3 * TC(iq) + 1) = -1;
    }
  }
}

//*----------------------------*
//|       File functions       |
//*----------------------------*

inline bool exists_file(const std::string &name) {
  struct stat buffer;
  return (stat(name.c_str(), &buffer) == 0);
}

//*----------------------------*
//|       Find functions       |
//*----------------------------*
inline uvec find_pulling_points(mat &x, double cstart, double length_step) {
  rowvec maxx = max(x);

  return find((abs(x.col(0) - cstart) < 0.5 * length_step) &&
              abs(x.col(1) - (maxx(1) / 2)) < 0.5 * length_step);
}

inline uvec find_dragline_points(mat &x, double cstart, double clength, double length_step) {
  rowvec maxx = max(x);

  return find(x.col(0) > cstart - length_step * 1E-1 &&
              x.col(0) < cstart + clength + length_step * 1E-1 &&
              abs(x.col(1) - (maxx(1) / 2)) < 0.5 * length_step);
}

inline uvec find_borders(mat &x, double length_step) {
  rowvec maxx = max(x);

  return find((abs(x.col(0)) < 0.5 * length_step) || (abs(x.col(1)) < 0.5 * length_step) ||
              (abs(x.col(0) - maxx(0)) < 0.5 * length_step) ||
              (abs(x.col(1) - maxx(1)) < 0.5 * length_step));
}

inline uvec find_front_border(mat &x, double length_step) {
  rowvec maxx = max(x);
  return find(abs(x.col(0)) < 0.5 * length_step);
}

inline uvec find_circle_around_point(mat &x, double xp, double yp, double radius, double length_step) {
  rowvec maxx = max(x);
  return find(sqrt(pow(x.col(0) - xp, 2) + pow(x.col(1) - yp, 2)) < (0.5 * length_step + radius));
}

inline uvec find_border(mat &x, double length_step, uword side, bool majmin) {
  rowvec maxx = max(x);
  if (majmin)
    return find(abs(x.col(side) - maxx(side)) > 0.5 * length_step);
  else
    return find(abs(x.col(side)) < 0.5 * length_step);
}

inline uvec find_rectangle(mat &x, double xs, double xe, double ys, double ye, double length_step) {
  return find(abs(x.col(0)) > xs - 0.5 * length_step && abs(x.col(0)) < xe + 0.5 * length_step &&
              abs(x.col(1)) > ys - 0.5 * length_step && abs(x.col(1)) < ye + 0.5 * length_step);
}

inline uvec find_pattern(mat &x, double w, double d_w, double angle, double length_step) {
  uvec F = zeros<uvec>(0);
  for (double np = -5; np < 5; np++) {
    F = join_cols(F, find((x.col(1) - tan(angle) * x.col(0)) - d_w / cos(angle) -
                                  np * (w + d_w) / cos(angle) + 0.4 * length_step >
                              0 &&
                          (x.col(1) - tan(angle) * x.col(0)) - (np + 1) * (w + d_w) / cos(angle) -
                                  0.4 * length_step <
                              0));
    ////abs(x.col(1)  + 1/tan(angle)*x.col(0)) > pl/sin(angle) - 0.4 * length_step &&
    ////abs(x.col(1)  + 1/tan(angle)*x.col(0)) < pl/sin(angle) + pl_d/sin(angle) - 0.4 * length_step
  }
  return F;
}

#endif /* DLF_arma */
