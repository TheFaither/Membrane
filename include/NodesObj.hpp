//
//  NodesObj.hpp
//  MP
//
//  Created by Daniele on 11/02/2017.
//  Copyright © 2017 DL. All rights reserved.
//

#ifndef Nodes_hpp
#define Nodes_hpp

#include "Params.hpp"

using namespace arma;
class NodesObj {

public:
  mat x;
  mat dmg;
  vec Ti;
  vec Tn;
  mat Ai;
  uvec stiffer;
  uvec attached;
  uvec init_att;
  umat sup;
  uword nx;
  uword ny;
  double N;
  double l0;

  NodesObj();
  NodesObj(uword nx, uword ny, double l0, double Ai, bool circular, double yd1, double yd2, double xd,
           double xl, bool with_diag);

  void copynodes(NodesObj Parent_nodes);
  double LinT(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i);
  double MLinT(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i);
  double dLinTd(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i, uword ia);
  double dLinTm(double kiI, double kII, double umaxz, double umaxxy, vec u, uword i, uword ia, uword ib);
  double ExpT(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i);
  double MExpT(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i);
  double dExpTd(double kiI, double kiII, double umaxz, double umaxxy, vec u, uword i, uword ia);
  double dExpTm(double kiI, double kII, double umaxz, double umaxxy, vec u, uword i, uword ia, uword ib);
  void moveNodes(const mat &u);
  void detach_geom_config(const mat &u);
  void attachNodes(umat VC);
  void detachNodes(umat VC);
  void set_attach(uword i, bool value);
  bool get_attach(uword i);
  void set_init_att(uword i, bool value);
  bool get_init_att(uword i);
  bool update_Ti_att(const Params &P, const vec &u, const vec &u_pre);
  void update_Tn_att(const vec &Qi_saved);
};

#endif /* Nodes_hpp */
