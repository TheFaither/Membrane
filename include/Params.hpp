#ifndef Params_hpp
#define Params_hpp

#include "DLF_arma.hpp"

using namespace arma;

class Params {

public:
  vec ic;
  std::string ID;
  double sidex;
  double sidey;
  double arcl;
  double nx, ny; // Nodes in x and y direction
  double l0;     // nodes spacing

  double GI;
  double GII;
  double umaxI;
  double umaxII;
  double thick;
  double opt_alpha;
  double opt_cd;
  double opt_cl;
  double opt_cE;

  double Ai;   // interface effective area = w * l
  double Tmax; //
  double kiI;  //
  double kiII; //

  double EA; // Elastic modulus

  double deps; // Increment

  mat u_result;
  mat T_result;
  mat Ti_result;

  std::string dir_result;
  std::string exe_dir;
  vec eps_result;    // Tracking variable
  mat sig_result;    // Tracking variable
  vec lambda_result; // Tracking variable

  Params(long int n_input, std::string ID, std::string exe_dir);
  mat input_cond();
  double arcl_cond();

  void set_params(long n_input);
  void init_results(const vec &u, const vec &T, const vec &Ti);
  void set_results(const vec &u, const vec &T, const vec &Ti, double eps, rowvec sig, double lambda,
                   uword stp);
  void expand_results();
  void save_results(bool &saved_once);
  void load_results();
  void set_loaded_results(vec &u, vec &T, vec &Ti, double &eps, rowvec &sig, double &lambda, uword stp);

  void deform(double ratio);

  std::string get_timestamp();
  std::string get_save_dir();
  std::string get_save_dir(std::string ID);
  void print_run_info();

  inline void adjust_EA_poisson() { EA *= 4 / 3; }
};

#endif /* Params_hpp */
