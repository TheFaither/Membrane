//
//  SpringsObj.hpp
//  MP
//
//  Created by Daniele on 11/02/2017.
//  Copyright © 2017 DL. All rights reserved.
//

#ifndef SpringsObj_hpp
#define SpringsObj_hpp

#include "NodesCollec.hpp"

using namespace arma;

class SpringsObj {

public:
  umat cm;
  mat EA;
  mat l0;
  double N;
  mat l;
  mat dl;
  vec T;
  uvec broken;

  SpringsObj();
  void SpringsNetwork(NodesObj Nodes, double E, double h, bool hex);
  void RandomizeStiffness();
  void IncreaseStiffness(NodesObj &Nodes, double coefficient, double ycondition, double xstart,
                         double clength);
  void IncreaseStiffness_find(NodesObj &Nodes, uvec FR, double coefficient);
  void updateSprings(NodesCollec NC, double E, double h);
  double getEA(uword i, uword j);
  bool checkBreak();
};

#endif /* Springs_hpp */
