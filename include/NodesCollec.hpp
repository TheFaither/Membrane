//
//  NodesCollec.hpp
//  MP
//
//  Created by Daniele on 12/02/2017.
//  Copyright © 2017 DL. All rights reserved.
//

#ifndef NodesCollec_hpp
#define NodesCollec_hpp

#include "NodesObj.hpp"
#include <stdio.h>

class NodesCollec {

public:
  double ALPHA;
  NodesObj Nodes;
  NodesObj Nodes_del;

  NodesCollec(uword nx, uword ny, double l0, double Ai, bool circular, double yd1, double yd2, double xd,
              double xl, bool with_diag);

  void create_stair(uword c1, uword c2);
};

#endif /* NodesCollec_hpp */
