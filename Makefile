MAKEFILE      = Makefile

####### Compiler, tools and options

CXX           = g++-9
CXXFLAGS      = -std=c++17 -Wall -O3 -march=native -I$(IDIR) -I/usr/local/include -I/usr/include/hdf5/serial -I/usr/local/opt/openblas/include
LDFLAGS       = -L/usr/local/lib/ -L/usr/lib/x86_64-linux-gnu/hdf5/serial -L/usr/local/opt/openblas/lib
LDLIBS        = -DARMA_USE_SUPERLU -DARMA_DONT_USE_OPENMP -DARMA_NO_DEBUG -larmadillo -lhdf5 -DARMA_DONT_USE_WRAPPER -llapack -lopenblas -lsuperlu
TARGET        = Membrane.out

.PHONY: all
all: $(TARGET)

.PHONY: debug
debug: CXXFLAGS += -ggdb
debug: $(TARGET)

####### Files
SDIR = ./src
src = $(wildcard $(SDIR)/*.cpp)


ODIR=$(SDIR)/obj
_obj = $(src:.cpp=.o)
obj = $(patsubst $(SDIR)/%,$(ODIR)/%,$(_obj))


IDIR =./include
_DEPS = $(wildcard $(IDIR)/*.hpp)



$(ODIR)/%.o: $(SDIR)/%.cpp $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS) -MMD

$(TARGET): $(obj)
		$(CXX) -o $@ $^ $(CXXFLAGS) $(LDFLAGS) $(LDLIBS)

.PHONY: clean
clean:
		rm -f $(obj) $(TARGET)
