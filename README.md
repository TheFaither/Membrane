# Membrane

A C++ Model to study the adhesion of elastic membranes. 

## Getting Started

### Prerequisites

This project makes use of the following libraries:
* make
* gcc-7 or newer
* armadillo
* LAPACK
* BLAS or openBLAS
* SuperLU



### Installing

#### Mac OS

You can install all the required libraries by using Homebrew

```
brew install gcc-9
brew install make
brew install armadillo
brew install openblas
brew install hdf5
```

#### Windows 10 
To install Membrane on Win10 you need to enable Windows Services for Linux on your Win10 and install Debian following [this guide](https://docs.microsoft.com/it-it/windows/wsl/install-win10).

The default installed Debian distro is Buster. However, the code needs the experimental features provided by bullseye.

Open /etc/sources.list:
```
sudo apt edit-sources
# OR open sources.list directly:
nano /etc/apt/sources.list
```
Replace “buster” with “bullseye” and “bullseye-security” for security updates. Now sources.list should look like this:
```
deb http://ftp.uk.debian.org/debian/ bullseye main contrib non-free
deb-src http://ftp.uk.debian.org/debian/ bullseye main contrib non-free

deb http://security.debian.org/debian-security bullseye-security/updates main
deb-src http://security.debian.org/debian-security bullseye-security/updates main
```
Update the system with
```
sudo apt upgrade
sudo apt dist-upgrade
```

Install the required libraries
```
sudo apt-get install make
sudo apt-get install gcc-9
sudo apt-get install libarmadillo-devel #automatically install required algebra libraries
sudo apt-get install libhdf5-dev
```


## Running

To run the code, rename "rename_to_input.csv" in the Input directory as "input.csv". 
```
make
./Membrane.out
```

To get help for the available flags, use
```
./Membrane.out -h
```
### Input file
```
1.4     #length of size x [mm]
1.5     #length of size y [mm]
0.04    #Normal adhesive energy [MPa * mm]
0.04    #Tangential adhesive energy [MPa * mm]
0.03    #Normal critical length [mm]
0.03    #Tangential critical length [mm]
0.0015  #Membrane thickness [mm] 
1680    #Membrane Young's modulus [MPa]
0.001   #displacement step (required but only used in displacement control) [mm]
160     #Pulling angle (facultative) [°]
0.5     #position X of the pulling point (facultative) [mm]
0.4     #Length of the dragline (Use 0.0 to have no dragline) (facultative) [mm]
15000   #Dragline Young's modulus (facultative) [MPa]
```